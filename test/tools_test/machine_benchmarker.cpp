#include "src/oprs/x86/local/local_impl.h"
#include "tools/benchmarker.h"
#include "gtest/gtest.h"

TEST(BENCHMARKER, MachineBenchmarker) {
  auto benchmarker = std::make_unique<MachineBenchmarker>();
  benchmarker->printSupportedHardware();
  benchmarker->printMachineFullMemorySpeed();
  benchmarker->printCoreNum();
  benchmarker->printHardwareSpeed();
}

TEST(BENCHMARKER, Benchmarker) {
  Benchmarker<x86::LocalForwardImpl> benchmarker;
}
