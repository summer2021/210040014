#include "benchmarker.h"
#include "src/oprs/naive/local/local_impl.h"
#include "src/oprs/x86/local/local_impl.h"
#include "src/oprs/x86/local/local_impl_opt1.h"
#include "src/oprs/x86/local/local_impl_opt2.h"
#include "gtest/gtest.h"
#include <iostream>

TEST(PERFORMANCE, LOCAL_NAIVE) {
  Benchmarker<naive::LocalForwardImpl> benchmarker;
  benchmarker.setTestTime(1);
  size_t N = 16;
  size_t IC = 16;
  size_t OC = 16;
  for (size_t IH : {300}) {
    for (size_t IW : {300}) {
      for (size_t FH : {2}) {
        for (size_t FW : {2}) {
          for (size_t PH : {1}) {
            for (size_t PW : {1}) {
              for (size_t SH : {1}) {
                for (size_t SW : {1}) {
                  benchmarker.m_param = {PH, PW, SH, SW};
                  auto OH = (IH + 2 * PH - FH) / SH + 1;
                  auto OW = (IW + 2 * PW - FW) / SW + 1;
                  benchmarker.exec(TensorLayout({N, IC, IH, IW}),
                                   TensorLayout({OH, OW, IC, FH, FW, OC}));
                }
              }
            }
          }
        }
      }
    }
  }
}

TEST(PERFORMANCE, LOCAL_OPT1) {
  Benchmarker<x86::LocalForwardOpt1Impl> benchmarker;
  benchmarker.setTestTime(1);
  size_t N = 16;
  size_t IC = 16;
  size_t OC = 16;
  for (size_t IH : {300}) {
    for (size_t IW : {300}) {
      for (size_t FH : {2}) {
        for (size_t FW : {2}) {
          for (size_t PH : {1}) {
            for (size_t PW : {1}) {
              for (size_t SH : {1}) {
                for (size_t SW : {1}) {
                  benchmarker.m_param = {PH, PW, SH, SW};
                  auto OH = (IH + 2 * PH - FH) / SH + 1;
                  auto OW = (IW + 2 * PW - FW) / SW + 1;
                  benchmarker.exec(TensorLayout({N, IC, IH, IW}),
                                   TensorLayout({OH, OW, IC, FH, FW, OC}));
                }
              }
            }
          }
        }
      }
    }
  }
}

TEST(PERFORMANCE, LOCAL_OPT) {
  Benchmarker<x86::LocalForwardImpl> benchmarker;
  benchmarker.setTestTime(1);
  size_t N = 16;
  size_t IC = 16;
  size_t OC = 16;
  for (size_t IH : {300}) {
    for (size_t IW : {300}) {
      for (size_t FH : {2}) {
        for (size_t FW : {2}) {
          for (size_t PH : {1}) {
            for (size_t PW : {1}) {
              for (size_t SH : {1}) {
                for (size_t SW : {1}) {
                  benchmarker.m_param = {PH, PW, SH, SW};
                  auto OH = (IH + 2 * PH - FH) / SH + 1;
                  auto OW = (IW + 2 * PW - FW) / SW + 1;
                  benchmarker.exec(TensorLayout({N, IC, IH, IW}),
                                   TensorLayout({OH, OW, IC, FH, FW, OC}));
                }
              }
            }
          }
        }
      }
    }
  }
}

TEST(PERFORMANCE, LOCAL_OPT2) {
  Benchmarker<x86::LocalForwardOpt2Impl> benchmarker;
  benchmarker.setTestTime(1);
  size_t N = 16;
  size_t IC = 16;
  size_t OC = 16;
  for (size_t IH : {300}) {
    for (size_t IW : {300}) {
      for (size_t FH : {2}) {
        for (size_t FW : {2}) {
          for (size_t PH : {1}) {
            for (size_t PW : {1}) {
              for (size_t SH : {1}) {
                for (size_t SW : {1}) {
                  benchmarker.m_param = {PH, PW, SH, SW};
                  auto OH = (IH + 2 * PH - FH) / SH + 1;
                  auto OW = (IW + 2 * PW - FW) / SW + 1;
                  benchmarker.exec(TensorLayout({N, IC, IH, IW}),
                                   TensorLayout({OH, OW, IC, FH, FW, OC}));
                }
              }
            }
          }
        }
      }
    }
  }
}