#include "src/oprs/naive/local/local_impl.h"
#include "src/oprs/x86/local/local_impl.h"
#include "tools/tool.h"
#include "gtest/gtest.h"
#include <iostream>

using namespace std;

namespace {
bool test_validity(lzdnn::LocalForward *base, lzdnn::LocalForward *target) {
  TensorShower tensorshower;
  for (size_t N : {1, 2, 4, 8, 16}) {
    for (size_t IC : {1, 2, 4, 8, 16}) {
      for (size_t IH : {4, 8, 16}) {
        for (size_t IW : {4, 8, 16}) {
          for (size_t FH : {2, 3}) {
            for (size_t FW : {2, 3}) {
              //   for (size_t PH : {0, 1, 2}) {
              //     for (size_t PW : {0, 1, 2}) {
              for (size_t PH : {1, 2}) {
                for (size_t PW : {1, 2}) {
                  for (size_t SH : {1}) {
                    for (size_t SW : {1}) {
                      for (size_t OC : {2, 3, 4, 5}) {
                        // create arr
                        // set param
                        base->setParam({PH, PW, SH, SW});
                        target->setParam({PH, PW, SH, SW});
                        auto src_ptr = new float[N * IC * IH * IW];
                        auto OH = (IH + 2 * PH - FH) / SH + 1;
                        auto OW = (IW + 2 * PW - FW) / SW + 1;
                        auto aim_ptr = new float[N * OC * OH * OW];
                        auto dst_ptr = new float[N * OC * OH * OW];
                        auto filter_ptr =
                            new float[OH * OW * IC * FH * FW * OC];

                        // create tensor and workspace
                        auto src_tensor = Tensor({N, IC, IH, IW}, src_ptr);
                        auto filter_tensor =
                            Tensor({OH, OW, IC, FH, FW, OC}, filter_ptr);
                        auto aim_tensor = Tensor({N, OC, OH, OW}, aim_ptr);
                        auto dst_tensor = Tensor({N, OC, OH, OW}, dst_ptr);

                        // init tensor
                        src_tensor.randn();
                        // filter_tensor.randn();
                        filter_tensor.set_value(1.f);
                        aim_tensor.set_zero();
                        dst_tensor.set_zero();
                        // run
                        auto aim_workspace_size = base->get_workspace_in_bytes(
                            src_tensor.layout, filter_tensor.layout);
                        auto dst_workspace_size =
                            target->get_workspace_in_bytes(
                                src_tensor.layout, filter_tensor.layout);
                        auto aim_workspace_ptr = new float[aim_workspace_size];
                        auto dst_workspace_ptr = new float[dst_workspace_size];
                        auto aim_workspace =
                            WorkSpace(aim_workspace_ptr, aim_workspace_size);
                        auto dst_workspace =
                            WorkSpace(dst_workspace_ptr, dst_workspace_size);
                        base->exec(src_tensor, filter_tensor, aim_tensor,
                                   aim_workspace);
                        target->exec(src_tensor, filter_tensor, dst_tensor,
                                     dst_workspace);
                        // test
                        auto equal_flag = equal_tensor(aim_tensor, dst_tensor);
                        if (!equal_flag) {
                          cout << "the result is error: " << endl;
                          cout << "the src shape is: " << endl;
                          tensorshower.show(src_tensor);
                          cout << "the filter shape is: " << endl;
                          tensorshower.show(filter_tensor);
                          cout << "the dst shape is: " << endl;
                          tensorshower.show(dst_tensor);
                          cout << "This is aim_tensor:" << endl;
                          tensorshower.print(aim_tensor);
                          cout << "This is dst_tensor:" << endl;
                          tensorshower.print(dst_tensor);
                        } else {
                          // cout << "true" << endl;
                        }
                        // cout << "This is aim_tensor:" << endl;
                        // tensorshower.print(aim_tensor);
                        // cout << "This is dst_tensor:" << endl;
                        // tensorshower.print(dst_tensor);
                        delete[] src_ptr;
                        delete[] aim_ptr;
                        delete[] dst_ptr;
                        delete[] filter_ptr;
                        if (!equal_flag)
                          return false;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
}
} // namespace

TEST(VALIDITY, LOCAL_X86) {
  auto baseKernel = new naive::LocalForwardImpl();
  auto optKernel = new x86::LocalForwardImpl();
  EXPECT_EQ(test_validity(baseKernel, optKernel), true);
}