#include "src/oprs/naive/local/local_impl.h"
#include "src/oprs/x86/local/local_impl_opt1.h"
#include "tools/tool.h"
#include "gtest/gtest.h"
#include <iostream>

using namespace std;

namespace {
bool test_validity(lzdnn::LocalForward *base, lzdnn::LocalForward *target) {
  TensorShower tensorshower;
  for (size_t N : {1, 2, 4, 8, 16}) {
    for (size_t IC : {1, 2, 4, 8, 16}) {
      for (size_t IH : {4, 8, 16}) {
        for (size_t IW : {4, 8, 16}) {
          for (size_t FH : {2, 3}) {
            for (size_t FW : {2, 3}) {
              //   for (size_t PH : {0, 1, 2}) {
              //     for (size_t PW : {0, 1, 2}) {
              for (size_t PH : {1, 2}) {
                for (size_t PW : {1, 2}) {
                  for (size_t SH : {1}) {
                    for (size_t SW : {1}) {
                      for (size_t OC : {2, 3, 4, 5}) {
                        // create arr
                        // set param
                        base->setParam({PH, PW, SH, SW});
                        target->setParam({PH, PW, SH, SW});
                        auto src_ptr = new float[N * IC * IH * IW];
                        auto OH = (IH + 2 * PH - FH) / SH + 1;
                        auto OW = (IW + 2 * PW - FW) / SW + 1;
                        auto aim_ptr = new float[N * OC * OH * OW];
                        auto dst_ptr = new float[N * OC * OH * OW];
                        auto filter_ptr =
                            new float[OH * OW * IC * FH * FW * OC];

                        // create tensor and workspace
                        auto src_tensor = Tensor({N, IC, IH, IW}, src_ptr);
                        auto filter_tensor =
                            Tensor({OH, OW, IC, FH, FW, OC}, filter_ptr);
                        auto aim_tensor = Tensor({N, OC, OH, OW}, aim_ptr);
                        auto dst_tensor = Tensor({N, OC, OH, OW}, dst_ptr);

                        // init tensor
                        src_tensor.set_value(1.f);
                        // filter_tensor.randn();
                        filter_tensor.randn();
                        aim_tensor.set_zero();
                        dst_tensor.set_zero();
                        // run
                        auto aim_workspace_size = base->get_workspace_in_bytes(
                            src_tensor.layout, filter_tensor.layout);
                        auto dst_workspace_size =
                            target->get_workspace_in_bytes(
                                src_tensor.layout, filter_tensor.layout);
                        auto aim_workspace_ptr = new float[aim_workspace_size];
                        auto dst_workspace_ptr = new float[dst_workspace_size];
                        auto aim_workspace =
                            WorkSpace(aim_workspace_ptr, aim_workspace_size);
                        auto dst_workspace =
                            WorkSpace(dst_workspace_ptr, dst_workspace_size);
                        base->exec(src_tensor, filter_tensor, aim_tensor,
                                   aim_workspace);
                        target->exec(src_tensor, filter_tensor, dst_tensor,
                                     dst_workspace);
                        // test
                        auto equal_flag = equal_tensor(aim_tensor, dst_tensor);
                        if (!equal_flag) {
                          cout << "the result is error: " << endl;
                          cout << "the src shape is: " << endl;
                          tensorshower.show(src_tensor);
                          cout << "the filter shape is: " << endl;
                          tensorshower.show(filter_tensor);
                          cout << "the dst shape is: " << endl;
                          tensorshower.show(dst_tensor);
                          cout << "This is aim_tensor:" << endl;
                          tensorshower.print(aim_tensor);
                          cout << "This is dst_tensor:" << endl;
                          tensorshower.print(dst_tensor);
                        } else {
                          // cout << "true" << endl;
                        }
                        // cout << "This is aim_tensor:" << endl;
                        // tensorshower.print(aim_tensor);
                        // cout << "This is dst_tensor:" << endl;
                        // tensorshower.print(dst_tensor);
                        delete[] src_ptr;
                        delete[] aim_ptr;
                        delete[] dst_ptr;
                        delete[] filter_ptr;
                        if (!equal_flag)
                          return false;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
}
} // namespace

TEST(VALIDITY, LOCAL_OPT1_X86) {
  auto baseKernel = new naive::LocalForwardImpl();
  auto optKernel = new x86::LocalForwardOpt1Impl();
  EXPECT_EQ(test_validity(baseKernel, optKernel), true);
}

void im2col(const float *A, float *B, const size_t PH, const size_t PW,
            size_t FH, const size_t FW, const size_t SH, const size_t SW,
            const size_t H1, const size_t W1, const size_t C1, const size_t H2,
            const size_t W2) {
  // check the size
  auto check_H2 = (H1 + 2 * PH - FH) / SH + 1;
  auto check_W2 = (W1 + 2 * PW - FW) / SW + 1;
  if (check_H2 != H2 || check_W2 != W2) {
    cout << "IH : " << H1 << endl;
    cout << "IW : " << W1 << endl;
    cout << "IC : " << C1 << endl;
    cout << "FH : " << FH << endl;
    cout << "FW : " << FW << endl;
    cout << "PH : " << PH << endl;
    cout << "PW : " << PW << endl;
    cout << "SH : " << SH << endl;
    cout << "SW : " << SW << endl;
    cout << "OH : " << H2 << endl;
    cout << "OW : " << W2 << endl;
    cout << "check_H2 : " << check_H2 << endl;
    cout << "check_W2 : " << check_W2 << endl;
    exit(-1);
  }

  // 对于每个Patch
  auto B_rowNum = C1 * FH * FW;
  for (size_t col_idx = 0; col_idx < B_rowNum; col_idx++) {
    auto w_offset = col_idx % FW;        //该列属于patch的哪一widthIdx
    auto h_offset = (col_idx / FW) % FH; //该列属于patch的哪一heightIdx
    auto c_offset = col_idx / (FH * FW); //该列属于patch的哪一chaneelIdx
    for (size_t h = 0; h < H2; h++) {
      for (size_t w = 0; w < W2; w++) {    //第[h,w]个patch
        auto patch_Ah = h_offset + h * SH; //第[h,w]个patch所在A中的heightIdx
        auto patch_Aw = w_offset + w * SW; //第[h,w]个patch所在A中的widthIdx
        // index = (col_idx * H2 * W2) + (h * W2 + w);
        // auto index = (col_idx * H2 + h) * W2 + w; //在B中的index
        auto index = (h * W2 + w) * B_rowNum + col_idx;
        auto real_patch_Ah = patch_Ah - PH;
        auto real_patch_Aw = patch_Aw - PW;
        if (real_patch_Ah >= H1 || real_patch_Aw >= W1)
          B[index] = 0;
        else
          B[index] = A[c_offset * H1 * W1 + real_patch_Ah * W1 + real_patch_Aw];
      }
    }
  }
}

void im2colt(const float *A, float *B, const size_t PH, const size_t PW,
             size_t FH, const size_t FW, const size_t SH, const size_t SW,
             const size_t H1, const size_t W1, const size_t C1, const size_t H2,
             const size_t W2) {
  // check the size
  auto check_H2 = (H1 + 2 * PH - FH) / SH + 1;
  auto check_W2 = (W1 + 2 * PW - FW) / SW + 1;
  if (check_H2 != H2 || check_W2 != W2) {
    cout << "IH : " << H1 << endl;
    cout << "IW : " << W1 << endl;
    cout << "IC : " << C1 << endl;
    cout << "FH : " << FH << endl;
    cout << "FW : " << FW << endl;
    cout << "PH : " << PH << endl;
    cout << "PW : " << PW << endl;
    cout << "SH : " << SH << endl;
    cout << "SW : " << SW << endl;
    cout << "OH : " << H2 << endl;
    cout << "OW : " << W2 << endl;
    cout << "check_H2 : " << check_H2 << endl;
    cout << "check_W2 : " << check_W2 << endl;
    exit(-1);
  }

  // 对于每个Patch
  auto B_rowNum = C1 * FH * FW;
  for (size_t col_idx = 0; col_idx < B_rowNum; col_idx++) {
    auto w_offset = col_idx % FW;        //该列属于patch的哪一widthIdx
    auto h_offset = (col_idx / FW) % FH; //该列属于patch的哪一heightIdx
    auto c_offset = col_idx / (FH * FW); //该列属于patch的哪一chaneelIdx
    for (size_t h = 0; h < H2; h++) {
      for (size_t w = 0; w < W2; w++) {    //第[h,w]个patch
        auto patch_Ah = h_offset + h * SH; //第[h,w]个patch所在A中的heightIdx
        auto patch_Aw = w_offset + w * SW; //第[h,w]个patch所在A中的widthIdx
        // index = (col_idx * H2 * W2) + (h * W2 + w);
        auto index = (col_idx * H2 + h) * W2 + w; //在B中的index
        auto real_patch_Ah = patch_Ah - PH;
        auto real_patch_Aw = patch_Aw - PW;
        if (real_patch_Ah >= H1 || real_patch_Aw >= W1)
          B[index] = 0;
        else
          B[index] = A[c_offset * H1 * W1 + real_patch_Ah * W1 + real_patch_Aw];
      }
    }
  }
}

TEST(VALIDITY, IM2COLT) {
  for (size_t IC : {1, 2, 4, 8, 16}) {
    for (size_t IH : {4, 8, 16}) {
      for (size_t IW : {4, 8, 16}) {
        for (size_t FH : {2, 3}) {
          for (size_t FW : {2, 3}) {
            //   for (size_t PH : {0, 1, 2}) {
            //     for (size_t PW : {0, 1, 2}) {
            for (size_t PH : {1, 2}) {
              for (size_t PW : {1, 2}) {
                for (size_t SH : {1}) {
                  for (size_t SW : {1}) {
                    for (size_t OC : {2, 3, 4, 5}) {
                      // create arr
                      // set param
                      auto src_ptr = new float[IC * IH * IW];
                      auto OH = (IH + 2 * PH - FH) / SH + 1;
                      auto OW = (IW + 2 * PW - FW) / SW + 1;
                      auto im2col_ptr = new float[IC * FW * FH * OH * OW];
                      auto im2colt_ptr = new float[IC * FW * FH * OH * OW];

                      // create tensor and workspace
                      auto src_tensor = Tensor({IC, IH, IW}, src_ptr);
                      auto im2col_tensor =
                          Tensor({OH * OW, IC * FW * FH}, im2col_ptr);
                      auto im2colt_tensor =
                          Tensor({IC * FW * FH, OH * OW}, im2colt_ptr);

                      // init tensor
                      src_tensor.randn();
                      // filter_tensor.randn();
                      im2col(src_ptr, im2col_ptr, PH, PW, FH, FW, SH, SW, IH,
                             IW, IC, OH, OW);
                      im2colt(src_ptr, im2colt_ptr, PH, PW, FH, FW, SH, SW, IH,
                              IW, IC, OH, OW);
                      EXPECT_EQ(equal_tensor_t(im2col_tensor, im2colt_tensor),
                                true);
                      // run
                      delete[] src_ptr;
                      delete[] im2col_ptr;
                      delete[] im2colt_ptr;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}