#include "src/oprs/naive/local/local_impl.h"
#include "tools/tool.h"
#include "gtest/gtest.h"
#include <iostream>

using namespace std;

namespace {
bool test_naive_local(void) {
  auto tensorShower = TensorShower();
  auto naiveLocalForward = naive::LocalForwardImpl();

#define src_matrix 1, 2, 3, 4, 4, 3, 2, 1
#define src_matrixs src_matrix, src_matrix, src_matrix
  float src_ptr[] = {src_matrixs, src_matrixs};
  auto src_tensor = Tensor({1, 3, 4, 4}, src_ptr);
  float filter_ptr[6 * 6 * 3 * 2];
  for (size_t i = 0; i < 6 * 6 * 3 * 2; i++)
    filter_ptr[i] = 1.0f;
  auto filter_tensor = Tensor({3, 3, 3, 2, 2, 2}, filter_ptr);
  naiveLocalForward.setParam({0, 0, 1, 1});
  float dst_ptr[18] = {0};
  auto dst_tensor = Tensor({1, 2, 3, 3}, dst_ptr);
  auto workspace = WorkSpace(nullptr, 0);
  naiveLocalForward.exec(src_tensor, filter_tensor, dst_tensor, workspace);
  //   for (size_t i = 0; i < 18; i++)
  //     cout << dst_ptr[i] << endl;
  return true;
}
} // namespace

TEST(VALIDITY, LOCAL_NAIVE) { EXPECT_EQ(test_naive_local(), true); }