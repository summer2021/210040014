#include "local_impl_opt1.h"
#include <cstdio>
#include <emmintrin.h>
#include <exception>
#include <immintrin.h>
#include <iostream>
#include <string.h>
#include <string>

using namespace std;
namespace {

#define KERNEL_ERROR(msg) throw KernelException(msg)
class KernelException : public std::exception {
public:
  KernelException(const std::string &msg) : m_msg(msg) {}
  const char *what() const noexcept { return m_msg.c_str(); }

private:
  std::string m_msg;
};

/**
 * @brief change A to B use im2col,and transpose
 * @param A    src matrix
 * @param B    dst matrix
 * @param PH   padding size in height
 * @param PW   padding size in width
 * @param FH   height of a kernel
 * @param FW   width of a kernel
 * @param SH   stride of h
 * @param SW   stride of w
 * @param C1   channel of A
 * @param H1   height of A
 * @param W1   width of A
 * @param H2   height of output
 * @param W2   width of output
 */

void im2colt(const float *A, float *B, const size_t PH, const size_t PW,
             size_t FH, const size_t FW, const size_t SH, const size_t SW,
             const size_t H1, const size_t W1, const size_t C1, const size_t H2,
             const size_t W2) {
  // check the size
  auto check_H2 = (H1 + 2 * PH - FH) / SH + 1;
  auto check_W2 = (W1 + 2 * PW - FW) / SW + 1;
  if (check_H2 != H2 || check_W2 != W2) {
    cout << "IH : " << H1 << endl;
    cout << "IW : " << W1 << endl;
    cout << "IC : " << C1 << endl;
    cout << "FH : " << FH << endl;
    cout << "FW : " << FW << endl;
    cout << "PH : " << PH << endl;
    cout << "PW : " << PW << endl;
    cout << "SH : " << SH << endl;
    cout << "SW : " << SW << endl;
    cout << "OH : " << H2 << endl;
    cout << "OW : " << W2 << endl;
    cout << "check_H2 : " << check_H2 << endl;
    cout << "check_W2 : " << check_W2 << endl;

    KERNEL_ERROR("the output size is wrong");
  }

  // 对于每个Patch
  auto B_rowNum = C1 * FH * FW;
  for (size_t col_idx = 0; col_idx < B_rowNum; col_idx++) {
    auto w_offset = col_idx % FW;        //该列属于patch的哪一widthIdx
    auto h_offset = (col_idx / FW) % FH; //该列属于patch的哪一heightIdx
    auto c_offset = col_idx / (FH * FW); //该列属于patch的哪一chaneelIdx
    for (size_t h = 0; h < H2; h++) {
      for (size_t w = 0; w < W2; w++) {    //第[h,w]个patch
        auto patch_Ah = h_offset + h * SH; //第[h,w]个patch所在A中的heightIdx
        auto patch_Aw = w_offset + w * SW; //第[h,w]个patch所在A中的widthIdx
        // index = (col_idx * H2 * W2) + (h * W2 + w);
        // auto index = (col_idx * H2 + h) * W2 + w; //在B中的index
        auto index = (h * W2 + w) * B_rowNum + col_idx;
        auto real_patch_Ah = patch_Ah - PH;
        auto real_patch_Aw = patch_Aw - PW;
        if (real_patch_Ah >= H1 || real_patch_Aw >= W1)
          B[index] = 0;
        else
          B[index] = A[c_offset * H1 * W1 + real_patch_Ah * W1 + real_patch_Aw];
      }
    }
  }
}

void vec_sum_dot(const float *A, const float *B, float *C, size_t M) {
  for (size_t m = 0; m < M; m++) {
    C[m] += A[m] * B[m];
  }
}
#define UNROLL_FUNC1(cb, a...) cb(0, ##a)
#define UNROLL_FUNC2(cb, a...) cb(0, ##a) cb(1, ##a)
#define UNROLL_FUNC3(cb, a...)                                                 \
  UNROLL_FUNC2(cb, ##a)                                                        \
  cb(2, ##a)
#define UNROLL_FUNC4(cb, a...)                                                 \
  UNROLL_FUNC3(cb, ##a)                                                        \
  cb(3, ##a)
#define UNROLL_FUNC5(cb, a...)                                                 \
  UNROLL_FUNC4(cb, ##a)                                                        \
  cb(4, ##a)
#define UNROLL_FUNC6(cb, a...)                                                 \
  UNROLL_FUNC5(cb, ##a)                                                        \
  cb(5, ##a)
#define UNROLL_FUNC7(cb, a...)                                                 \
  UNROLL_FUNC6(cb, ##a)                                                        \
  cb(6, ##a)
#define UNROLL_FUNC8(cb, a...)                                                 \
  UNROLL_FUNC7(cb, ##a)                                                        \
  cb(7, ##a)
#define UNROLL_FUNC(cb, i, a...) UNROLL_FUNC##i(cb, ##a)

void vec_sum_dot_opt0(const float *A, const float *B, float *C, size_t M) {
  size_t m = 0;
#define LOADA(i) __m256 A##i = _mm256_loadu_ps(A + m + (8 * i));
#define LOADB(i) __m256 B##i = _mm256_loadu_ps(B + m + (8 * i));
#define LOADC(i) __m256 C##i = _mm256_loadu_ps(C + m + (8 * i));
#define CALADD(i) C##i = _mm256_fmadd_ps(A##i, B##i, C##i);
#define STORE(i) _mm256_storeu_ps(C + m + (8 * i), C##i);
#define EXE_BLOCK(i)                                                           \
  UNROLL_FUNC(LOADA, i)                                                        \
  UNROLL_FUNC(LOADB, i)                                                        \
  UNROLL_FUNC(LOADC, i)                                                        \
  UNROLL_FUNC(CALADD, i)                                                       \
  UNROLL_FUNC(STORE, i)

  for (; m + 64 <= M; m += 64) {
    EXE_BLOCK(8)
  }
  if (m + 32 <= M) {
    EXE_BLOCK(4)
    m += 32;
  }
  if (m + 16 <= M) {
    EXE_BLOCK(2)
    m += 16;
  }
  if (m + 8 <= M) {
    EXE_BLOCK(1)
    m += 8;
  }
  for (; m < M; m++) {
    C[m] += A[m] * B[m];
  }
}
} // namespace

namespace x86 {
LocalForwardOpt1Impl::allParam
LocalForwardOpt1Impl::make_param(tensor_in src, tensor_in filter, tensor_in dst,
                                 WorkSpace &workspace) {
  return {src.raw_ptr, filter.raw_ptr, dst.raw_ptr, workspace.raw_ptr,
          src.layout.shape[0], src.layout.shape[1], src.layout.shape[2],
          src.layout.shape[3], dst.layout.shape[1], dst.layout.shape[2],
          dst.layout.shape[3], filter.layout.shape[3], filter.layout.shape[4],
          // pad_h,pad_w,stride_h,stride_w
          m_param.ph, m_param.pw, m_param.sh, m_param.sw,
          // input_batch_size,output_batch_size
          src.layout.shape[1] * src.layout.shape[2] * src.layout.shape[3],
          dst.layout.shape[1] * dst.layout.shape[2] * dst.layout.shape[3]};
}

void LocalForwardOpt1Impl::exec(tensor_in src, tensor_in filter, tensor_out dst,
                                WorkSpace &workspace) {
  //首先先im2col
  auto kernal_param = make_param(src, filter, dst, workspace);
  naive_kern(kernal_param);
}
void LocalForwardOpt1Impl::naive_kern(
    LocalForwardOpt1Impl::allParam &kernal_param) {
#define UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(_p, _dtype)              \
  const _dtype *src = static_cast<const _dtype *>(_p.src);                     \
  const _dtype *filter = static_cast<const _dtype *>(_p.filter);               \
  _dtype *dst = static_cast<_dtype *>(_p.dst);                                 \
  _dtype *XI = static_cast<_dtype *>(_p.workspace);                            \
  const size_t N = _p.n, IC = _p.ic, IH = _p.ih, IW = _p.iw, OC = _p.oc,       \
               OH = _p.oh, OW = _p.ow, FH = _p.fh, FW = _p.fw;                 \
  const size_t PH = _p.ph, PW = _p.pw, SH = _p.sh, SW = _p.sw;                 \
  const size_t INP_BS = _p.inp_bs, OUT_BS = _p.out_bs
  UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(kernal_param, float);
  float *out_workspace = XI + FH * FW * IC * OH * OW + 16;
  for (size_t n = 0; n < N; n++) { //对于每个batch
    auto src_n = src + n * INP_BS; //第n个batch的输入
    auto dst_n = dst + n * OUT_BS;
    size_t XI_colNum = IC * FH * FW;
    size_t XI_rowNum = OH * OW;
    // memset(out_workspace, 0, sizeof(float) * FH * FW * IC * OC);
    im2colt(src_n, XI, PH, PW, FH, FW, SH, SW, IH, IW, IC, OH, OW);
    // XI : im2col后的XI
    memset(dst_n, 0, sizeof(float) * OUT_BS);
//方案1 12s
// for (size_t row_idx = 0; row_idx < XI_rowNum; row_idx++) {
//   auto src_row_ptr = XI + row_idx * XI_colNum;
//   auto filter_row_ptr = filter + row_idx * XI_colNum * OC;
//   for (size_t elem_idx = 0; elem_idx < XI_colNum * OC; elem_idx++) {
//     auto src_elem_idx = elem_idx / OC; //第几个
//     auto oc_idx = elem_idx % OC;       //第几个通道
//     dst_n[oc_idx * OH * OW + row_idx] +=
//         src_row_ptr[src_elem_idx] * filter_row_ptr[elem_idx];
//   }
// }
//方案3 2.7s
// for (size_t row_idx = 0; row_idx < XI_rowNum; row_idx++) {
//   auto src_row_ptr = XI + row_idx * XI_colNum;
//   auto filter_row_ptr = filter + row_idx * XI_colNum * OC;
//   for (size_t oc_idx = 0; oc_idx < OC; oc_idx++) {
//     for (size_t elem_idx = 0; elem_idx < XI_colNum; elem_idx++) {

//       dst_n[oc_idx * OH * OW + row_idx] +=
//           src_row_ptr[elem_idx] * filter_row_ptr[elem_idx * OC + oc_idx];
//     }
//   }
// }
//方案4 2.1s
#define DELARE_SRC(i) auto src_row_ptr##i = XI + (row_idx + i) * XI_colNum;
#define DELARE_FILTER(i)                                                       \
  auto filter_row_ptr##i = filter + (row_idx + i) * XI_colNum * OC;
#define SET_DST(i)                                                             \
  dst_n[oc_idx * OH * OW + row_idx + i] +=                                     \
      src_row_ptr##i[elem_idx] * filter_row_ptr##i[elem_idx * OC + oc_idx];
    size_t row_idx = 0;
    for (; row_idx + 4 <= XI_rowNum; row_idx += 4) {
      UNROLL_FUNC(DELARE_SRC, 4)
      UNROLL_FUNC(DELARE_FILTER, 4)
      for (size_t oc_idx = 0; oc_idx < OC; oc_idx++) {
        for (size_t elem_idx = 0; elem_idx < XI_colNum; elem_idx++) {
          UNROLL_FUNC(SET_DST, 4)
        }
      }
    }
    for (; row_idx < XI_rowNum; row_idx++) {
      UNROLL_FUNC(DELARE_SRC, 1)
      UNROLL_FUNC(DELARE_FILTER, 1)
      for (size_t oc_idx = 0; oc_idx < OC; oc_idx++) {
        for (size_t elem_idx = 0; elem_idx < XI_colNum; elem_idx++) {
          UNROLL_FUNC(SET_DST, 1)
        }
      }
    }
    //方案2 7s
    // for (size_t oc = 0; oc < OC; oc++) {
    //   for (size_t row_idx = 0; row_idx < XI_rowNum; row_idx++) {
    //     auto src_row_ptr = XI + row_idx * XI_colNum;
    //     auto filter_row_ptr = filter + row_idx * XI_colNum * OC;
    //     float acc = 0.f;
    //     for (size_t elem_idx = 0; elem_idx < XI_colNum; elem_idx++) {
    //       dst_n[oc * OH * OW + row_idx] +=
    //           src_row_ptr[elem_idx] * filter_row_ptr[elem_idx * OC + oc];
    //     }
    //   }
    // }
  }

#undef UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM
}
size_t
LocalForwardOpt1Impl::get_workspace_in_bytes(const TensorLayout &src,
                                             const TensorLayout &filter) {
  auto IC = src.shape[1];
  auto IH = src.shape[2];
  auto IW = src.shape[3];
  auto FH = filter.shape[3];
  auto FW = filter.shape[4];
  auto SH = m_param.sh;
  auto SW = m_param.sw;
  auto PH = m_param.ph;
  auto PW = m_param.pw;
  auto OH = (IH + 2 * PH - FH) / SH + 1;
  auto OW = (IW + 2 * PW - FW) / SW + 1;
  auto OC = filter.shape[5];
  return FH * FW * IC * OH * OW * sizeof(float) +
         FH * FW * IC * OC * sizeof(float) + 64;
}

// TODO:完成colt2im的编写
// TODO:用perf性能统计各函数执行时间
// TODO:完成backward的测试与编写
} // namespace x86
