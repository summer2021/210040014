#include "local_impl_opt2.h"
#include <cstdio>
#include <emmintrin.h>
#include <exception>
#include <immintrin.h>
#include <iostream>
#include <string.h>
#include <string>

using namespace std;
#define UNROLL_FUNC1(cb, a...) cb(0, ##a)
#define UNROLL_FUNC2(cb, a...) cb(0, ##a) cb(1, ##a)
#define UNROLL_FUNC3(cb, a...)                                                 \
  UNROLL_FUNC2(cb, ##a)                                                        \
  cb(2, ##a)
#define UNROLL_FUNC4(cb, a...)                                                 \
  UNROLL_FUNC3(cb, ##a)                                                        \
  cb(3, ##a)
#define UNROLL_FUNC(cb, i, a...) UNROLL_FUNC##i(cb, ##a)

namespace x86 {
LocalForwardOpt2Impl::allParam
LocalForwardOpt2Impl::make_param(tensor_in src, tensor_in filter, tensor_in dst,
                                 WorkSpace &workspace) {
  return {src.raw_ptr, filter.raw_ptr, dst.raw_ptr, workspace.raw_ptr,
          src.layout.shape[0], src.layout.shape[1], src.layout.shape[2],
          src.layout.shape[3], dst.layout.shape[1], dst.layout.shape[2],
          dst.layout.shape[3], filter.layout.shape[3], filter.layout.shape[4],
          // pad_h,pad_w,stride_h,stride_w
          m_param.ph, m_param.pw, m_param.sh, m_param.sw,
          // input_batch_size,output_batch_size
          src.layout.shape[1] * src.layout.shape[2] * src.layout.shape[3],
          dst.layout.shape[1] * dst.layout.shape[2] * dst.layout.shape[3]};
}

void LocalForwardOpt2Impl::exec(tensor_in src, tensor_in filter, tensor_out dst,
                                WorkSpace &workspace) {
  //首先先im2col
  auto kernal_param = make_param(src, filter, dst, workspace);
  naive_kern(kernal_param);
}
void LocalForwardOpt2Impl::naive_kern(
    LocalForwardOpt2Impl::allParam &kernal_param) {
#define UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(_p, _dtype)              \
  const _dtype *src = static_cast<const _dtype *>(_p.src);                     \
  const _dtype *filter = static_cast<const _dtype *>(_p.filter);               \
  _dtype *dst = static_cast<_dtype *>(_p.dst);                                 \
  _dtype *XI = static_cast<_dtype *>(_p.workspace);                            \
  const size_t N = _p.n, IC = _p.ic, IH = _p.ih, IW = _p.iw, OC = _p.oc,       \
               OH = _p.oh, OW = _p.ow, FH = _p.fh, FW = _p.fw;                 \
  const size_t PH = _p.ph, PW = _p.pw, SH = _p.sh, SW = _p.sw;                 \
  const size_t INP_BS = _p.inp_bs, OUT_BS = _p.out_bs
  UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(kernal_param, float);
  float *out_workspace = XI + FH * FW * IC * OH * OW + 16;
  for (size_t n = 0; n < N; n++) { //对于每个batch
    auto src_n = src + n * INP_BS; //第n个batch的输入
    auto dst_n = dst + n * OUT_BS;
    size_t XI_colNum = IC * FH * FW;
    size_t XI_rowNum = OH * OW;
    // 对于每个Patch
    auto B = XI;
    auto A = src_n;
    auto B_colNum = IC * FH * FW;
    auto B_rowNum = OH * OW;
    memset(dst_n, 0, sizeof(float) * OUT_BS);
// 方案6
#define SET_H_W(i) auto h##i = (row_idx + i) / OW, w##i = (row_idx + i) % OW;
#define SET_PATCH_H(i) auto patch_Ah##i = h_offset + h##i * SH - PH; 
#define SET_PATCH_W(i) auto patch_Aw##i = w_offset + w##i * SW - PW;
#define FILL_B(i)                                                              \
  if (patch_Ah##i >= IH || patch_Aw##i >= IW)                                  \
    B[col_idx + B_colNum * i] = 0;                                             \
  else                                                                         \
    B[col_idx + B_colNum * i] =                                                \
        A[c_offset * IH * IW + patch_Ah##i * IW + patch_Aw##i];

#define DELARE_SRC(i) auto src_row_ptr##i = B + i * B_colNum;
#define DELARE_FILTER(i)                                                       \
  auto filter_row_ptr##i = filter + (row_idx + i) * B_colNum * OC;
#define SET_DST(i)                                                             \
  dst_n[oc_idx * OH * OW + row_idx + i] +=                                     \
      src_row_ptr##i[elem_idx] * filter_row_ptr##i[elem_idx * OC + oc_idx];
#define EXEC(i)                                                                \
  for (; row_idx + i <= B_rowNum; row_idx += i) {                              \
    UNROLL_FUNC(SET_H_W, i)                                                    \
    for (size_t col_idx = 0; col_idx < B_colNum; col_idx++) {                  \
      auto w_offset = col_idx % FW;                                            \
      auto h_offset = (col_idx / FW) % FH;                                     \
      auto c_offset = col_idx / (FH * FW);                                     \
      UNROLL_FUNC(SET_PATCH_H, i)                                              \
      UNROLL_FUNC(SET_PATCH_W, i)                                              \
      UNROLL_FUNC(FILL_B, i)                                                   \
    }                                                                          \
    UNROLL_FUNC(DELARE_SRC, i)                                                 \
    UNROLL_FUNC(DELARE_FILTER, i)                                              \
    for (size_t oc_idx = 0; oc_idx < OC; oc_idx++) {                           \
      for (size_t elem_idx = 0; elem_idx < B_colNum; elem_idx++) {             \
        UNROLL_FUNC(SET_DST, i)                                                \
      }                                                                        \
    }                                                                          \
  }
    size_t row_idx = 0;
    EXEC(4);
    EXEC(2);
    EXEC(1);
  }
#undef SET_H_W
#undef SET_PATCH_H
#undef SET_PATCH_W
#undef FILL_B
#undef DELARE_FILTER
#undef DELARE_SRC
#undef SET_DST
#undef EXEC
#undef UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM
#undef UNROLL_FUNC
#undef UNROLL_FUNC1
#undef UNROLL_FUNC2
#undef UNROLL_FUNC3
#undef UNROLL_FUNC4
}
size_t LocalForwardOpt2Impl::get_workspace_in_bytes(const TensorLayout &src,
                                             const TensorLayout &filter) {
  auto IC = src.shape[1];
  auto FH = filter.shape[3];
  auto FW = filter.shape[4];
  return FH * FW * IC * 4 * sizeof(float) + 64;
}

} // namespace x86
