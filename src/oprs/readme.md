# 支持的算子

## 1. 卷积算子

+ general convolution：
+ depthwise separable convolution：
+ local share convolution：
+ local convolution：

## 2. 通用算子

+ matrix multiplication
+ matrix subtraction
+ matrix addition
+ reduce
