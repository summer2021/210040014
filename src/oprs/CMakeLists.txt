
file(GLOB_RECURSE SOURCES naive/*.cpp x86/*.cpp)
include_directories(${PROJECT_SOURCE_DIR})
set(CMAKE_CXX_FLAGS " -Ofast -march=native -mavx2 -mfma -ggdb")
add_library(OPRSET ${SOURCES})

target_include_directories(OPRSET PUBLIC "naive" "x86")