#include "local_impl.h"

namespace naive {
LocalForwardImpl::allParam LocalForwardImpl::make_param(tensor_in src,
                                                        tensor_in filter,
                                                        tensor_in dst,
                                                        WorkSpace &workspace) {
  return {src.raw_ptr, filter.raw_ptr, dst.raw_ptr, workspace.raw_ptr,
          src.layout.shape[0], src.layout.shape[1], src.layout.shape[2],
          src.layout.shape[3], dst.layout.shape[1], dst.layout.shape[2],
          dst.layout.shape[3], filter.layout.shape[3], filter.layout.shape[4],
          // pad_h,pad_w,stride_h,stride_w
          m_param.ph, m_param.pw, m_param.sh, m_param.sw,
          // input_batch_size,output_batch_size
          src.layout.shape[1] * src.layout.shape[2] * src.layout.shape[3],
          dst.layout.shape[1] * dst.layout.shape[2] * dst.layout.shape[3]};
}
#define UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(_p, _dtype)              \
  const _dtype *src = static_cast<const _dtype *>(_p.src);                     \
  const _dtype *filter = static_cast<const _dtype *>(_p.filter);               \
  _dtype *dst = static_cast<_dtype *>(_p.dst);                                 \
  _dtype *workspace = static_cast<_dtype *>(_p.workspace);                     \
  const size_t N = _p.n, IC = _p.ic, IH = _p.ih, IW = _p.iw, OC = _p.oc,       \
               OH = _p.oh, OW = _p.ow, FH = _p.fh, FW = _p.fw;                 \
  const size_t PH = _p.ph, PW = _p.pw, SH = _p.sh, SW = _p.sw;                 \
  const size_t INP_BS = _p.inp_bs, OUT_BS = _p.out_bs

void LocalForwardImpl::exec(tensor_in src, tensor_in filter, tensor_out dst,
                            WorkSpace &workspace) {
  auto kernal_param = make_param(src, filter, dst, workspace);
  naive_kern(kernal_param);
}

void LocalForwardImpl::naive_kern(LocalForwardImpl::allParam &kernal_param) {
  UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(kernal_param, float);
#define rep(n, N) for (size_t n = 0; n < N; n++)
  // 第n个batch第oc个通道第oh行第ow列
  rep(n, N) rep(oc, OC) rep(oh, OH) rep(ow, OW) {
    // 目标元素IC个filter参与计算
    auto &dval = dst[n * OUT_BS + oc * OH * OW + oh * OW + ow];
    // dval = sum(ic个(filter和src对应区域的点乘和))
    rep(fh, FH) rep(fw, FW) {
      size_t ih = SH * oh + fh; // src对应区域中对应(fh,fw)元素的行号(用于点乘)
      size_t iw = SW * ow + fw; // src对应区域中对应(fh,fw)元素的列号(用于点乘)
      ih -= PH, iw -= PW;
      //因为ih和iw是unsigned类型，所以padding的那些ih和iw会溢出，不满足下面条件
      if (ih < static_cast<size_t>(IH) && iw < static_cast<size_t>(IW)) {
        rep(ic, IC) {
          auto sval = src[n * INP_BS + ic * IH * IW + ih * IW + iw];
          // 因为对IC个input和OC个output来说，总共有IC*OC个filter（对正常的conv来说）
          // 但是对local conv而言，filter权值不共享，因此有 OW*OH*IC*OC个filter
          auto fval =
              filter[oh * OW * IC * FH * FW * OC + ow * IC * FH * FW * OC +
                     ic * FH * FW * OC + fh * FW * OC + fw * OC + oc];
          dval += sval * fval;
        }
      }
    }
  }
#undef rep
}

#undef UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM
} // namespace naive
