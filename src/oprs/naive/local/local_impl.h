#pragma once
#include "include/nn.h"
#include <cstddef>

//前向传播
namespace naive {
class LocalForwardImpl : public lzdnn::LocalForward {
private:
  struct allParam {
    void *src;
    void *filter;
    void *dst;
    void *workspace;
    size_t n, ic, ih, iw, oc, oh, ow, fh, fw, ph, pw, sh, sw;
    size_t inp_bs, out_bs;
  };
  allParam make_param(tensor_in src, tensor_in filter, tensor_in dst,
                      WorkSpace &workspace);

  void naive_kern(allParam &kernal_param);

public:
  LocalForwardImpl() {
    name = "LocalForward";
    m_param = {1, 1, 1, 1};
  };
  ~LocalForwardImpl(){};
  void exec(tensor_in src, tensor_in filter, tensor_out dst,
            WorkSpace &workspace) override;
  size_t get_workspace_in_bytes(const TensorLayout &src,
                                const TensorLayout &filter) override {
    return 0;
  }
};
} // namespace naive
