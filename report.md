# Summer 2021 终期报告

## 项目信息：

+ 申请项⽬目社区：MegEngine（天元）
+ 申请项⽬目名称：x86 中添加并优化 local

## 项目描述

目前 MegEngine 中 naive 和 CUDA 中支持了 local 和 local share 算子，但是 x86 中并没有支持，因此这些算子在 x86 上运行只能使用 naive 的算子，速度很慢。需要进行优化并达到x86能达到的极限性能。

## 项目实现方案：

首先需要跑通天元的MegDNN框架，能对算子进行测试，接着做一个local算子的基准测试程序，以保证后续local算子编写的正确性。接着利用x86下的simd等硬件运算器对local算子进行加速，查看 Intrinsics手册 编写加速代码，并在编写代码的过程中优化内存和计算速度。

<img src="picture/image-20210608110837129.png" width = "550" height = "400" alt="Intrinsics手册" align=center />

## 项目时间安排：

|   时间   |                         任务                         |
| :-------: | :--------------------------------------------------: |
|  7.1-7.8  |              跑通MegDNN下的算子测试代码              |
| 7.8-7.15 |               阅读有关local算子的资料               |
| 7.15-7.25 | 写一个local算子的naive版本，用于作为基准程序后续对比 |
| 7.25-8.2 |           熟悉Intrinsics手册和编写simd代码           |
|  8.3-8.9  |             设计local算子的simd加速结构             |
| 8.10-8.15 |         书写一版带有simd加速的local算子代码         |
| 8.16-8.30 |            查阅资料并改进原有simd加速代码            |
| 9.1-9.15 |         测试该算子是否逼近极限速度并不断改进         |
| 9.16-9.30 |                  进行最后测试和结题                  |

## 项目进度

已完成工作：

+ 跑通了MegDNN下的算子测试代码
+ 借鉴了MegDNN下的local算子的naive版本代码，在本地编写了一版naive版本的代码
+ 设计了几种加速方案，并进行实现，现在最快的版本能提高4.5倍的速度。

遇到的问题及解决方案：

+ 因为笔者并不是在MegDNN上进行测试，而是借鉴MegDNN实现了一版能够测试算子的架构，因此本项目有很多“多余的”代码，主要用于搭建架构。这为笔者后续优化更多算子建立了基础。
+ 笔者刚开始拿到这个题目的时候，首先用数学公式分析了一下local算子的具体计算公式，因此需要对深度学习的计算图有一个整体的认识，为此，笔者学习后专门为此写了一个博客https://zhuanlan.zhihu.com/p/393463009。
+ 在优化算子的过程中，笔者为了分析算子的实现性能，需要知道x86机器的各项硬件的性能峰值，因此，笔者专门为此编写了代码对x86机器的bandwidth，fma等运算单元进行峰值测试。
+ 一开始，笔者找到了一个非常好的方案，理论上来说应该是最快的实现，但是和导师沟通之后发现，filter的数据布局不能够改变，因此笔者只能够重新构思方案。

## 项目具体分析

### 1. local 算子调研报告

#### 1.1 local 算子 的forward

local算子是普通卷积算子的一个变形，普通的卷积算子通过共享卷积核实现参数上的减少，而local算子和普通的卷积算子做法恰好相反，没有任何一个感受野之间共享权值。以下默认$stride=1,padding=0$。

普通的卷积算子，前向传播的计算过程如下图所示：

![](./picture/conv算子.jpg "conv算子")

其中，左边的是源，一般的数据格式为 `[N,C,H,W]`，上图还有一个 batch 信息未画出，大家一般都用 NCHW 来描述它的数据格式，同样的，输出也是这个格式。（毕竟对用户来说，这种格式最为友好）但是能更好利用cache，更充分发挥计算性能的数据格式是 NCHW44 和 NCHW88 。

在上图我们可以清楚的见到，对于一个 batch 的输入，假设输入的通道数为 $C_1$，输出的通道数为$ C_2$，一个卷积核的大小为$h * w$，那么输入和输出之间就总共有$C_1 * C_2 * h*w$个权值。也就是说其中总共有$C_1*C_2$个卷积核。

而变形后的local算子，计算过程如下图所示：

![local算子](./picture/local算子.png)

由上图可以看出，local算子的输入和输出维度和普通卷积并没什么区别。区别就只在卷积核中，local算子不共享任何的卷积核，每个感受野都对应着一个独有的卷积核。因此，对于local来说，他的权值数量比起普通卷积核来说呈爆炸式增长。对于一个batch来说，假设输入的通道数为 $C_1$，输出的通道数为$ C_2$，一个卷积核的大小为$h * w$，那么输入和输出之间就总共有$C_1 * C_2 * OH*OW*h*w$个权值。（其中的$OH*OW$代表输出矩阵的高和宽）。因此，假如我们依然认为一个卷积核大小为$h*w$，那么输入和输出之间的卷积核数量就为：$C_1*C_2*OH*OW$。

#### 1.2 local 算子的 backward

假设我们有如下的计算图模型：

![计算图框架naive版](./picture/backward_infer.png)

具体关于计算图的模型以及推导可以看笔者的[知乎博客](https://zhuanlan.zhihu.com/p/393463009)，其中的大写字母代表残差，小写字母代表输出，我们可以通过全微分得到许多算子的反向计算公式。

对于 `local`算子而言，有：

![matrix_mul_add](picture/local_kernel.png)

首先根据上述的符号定义，我们可以知道$x$和$w$的维度都是$[c1*FH,FW,h_2*w_2]$，为了方便，这里先将他们设为$[h,w]$。

通过全微分推导有：

$$
dg = tr((\frac{\partial g}{\partial y})^Tdy_{w*1})=tr((\frac{\partial g}{\partial y})^Td((x_{h*w}\odot w_{h*w})^T1_{h*1}))) \\
= tr((\frac{\partial g}{\partial y})^Td(x_{h*w}\odot w_{h*w})^T1_{h*1})\\
=tr(1_{1*h}d(x_{h*w}\odot w_{h*w})(\frac{\partial g}{\partial y}))\\
=tr(1_{1*h}(dx_{h*w}\odot w_{h*w} + x_{h*w}\odot dw_{h*w})(\frac{\partial g}{\partial y})) \\
=tr(1_{1*h}(dx_{h*w}\odot w_{h*w})(\frac{\partial g}{\partial y}) + 1_{1*h}(x_{h*w}\odot dw_{h*w})(\frac{\partial g}{\partial y})) \\
=tr((\frac{\partial g}{\partial y})1_{1*h}(dx_{h*w}\odot w_{h*w}) + (\frac{\partial g}{\partial y})1_{1*h}(x_{h*w}\odot dw_{h*w})) \\ 
=tr((\frac{\partial g}{\partial y})1_{1*h}(w_{h*w}\odot dx_{h*w}) + (\frac{\partial g}{\partial y})1_{1*h}(x_{h*w}\odot dw_{h*w})) \\
=tr(((1_{h*1}(\frac{\partial g}{\partial y})^T) \odot w_{h*w})dx_{h*w} + ((1_{h*1}(\frac{\partial g}{\partial y})^T) \odot x_{h*w})dw_{h*w})
$$

故有：

$$
XI =\frac{\partial g}{\partial xi} = ((1_{h*1}(\frac{\partial g}{\partial y})^T) \odot w_{h*w})^T=(1_{h*1} Y^T \odot w_{h*w})^T\\
W = \frac{\partial g}{\partial xi} = ((1_{h*1}(\frac{\partial g}{\partial y})^T) \odot x_{h*w})^T=(1_{h*1} Y^T \odot x_{h*w})^T \\
$$

而我们的 `im2col`算子只不过对$x$做了数据重排，因此，计算X也非常简单，只需要做一个 `col2im`即可。

### 2. MegDNN 下的 local 算子的 forward 实现

```c++
#define UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(_p, _dtype)              \
  const _dtype *src = static_cast<const _dtype *>(_p.src);                     \
  const _dtype *filter = static_cast<const _dtype *>(_p.filter);               \
  _dtype *dst = static_cast<_dtype *>(_p.dst);                                 \
  _dtype *workspace = static_cast<_dtype *>(_p.workspace);                     \
  const size_t N = _p.n, IC = _p.ic, IH = _p.ih, IW = _p.iw, OC = _p.oc,       \
               OH = _p.oh, OW = _p.ow, FH = _p.fh, FW = _p.fw;                 \
  const size_t PH = _p.ph, PW = _p.pw, SH = _p.sh, SW = _p.sw;                 \
  const size_t INP_BS = _p.inp_bs, OUT_BS = _p.out_bs

template<bool is_xcorr, typename dtype>
void LocalForwardImpl::naive_kern(const FloatNoncontigBatchKernParam &param) {
    UNPACK_LOCAL_FLOAT_NONCONTIG_BATCH_KERN_PARAM(param, dtype);

    static_cast<void>(workspace);
    rep(n, N) rep(oc, OC) rep(oh, OH) rep(ow, OW) {
        auto &dval = dst[n*OUT_BS + oc*OH*OW + oh*OW + ow];
        dval = 0.0f;
        rep(fh, FH) rep(fw, FW) {
            size_t ih = SH*oh;
            size_t iw = SW*ow;
            if (is_xcorr) {
                ih += fh;
                iw += fw;
            } else {
                ih += FH-fh-1;
                iw += FW-fw-1;
            }
            ih -= PH;
            iw -= PW;
            if (ih < static_cast<size_t>(IH) && iw < static_cast<size_t>(IW)) {
                rep(ic, IC)  {
                    auto sval = src[n*INP_BS + ic*IH*IW + ih*IW + iw];
                    auto fval = filter[oh*OW*IC*FH*FW*OC + ow*IC*FH*FW*OC +
                        ic*FH*FW*OC + fh*FW*OC + fw*OC + oc];
                    dval += sval * fval;
                }
            }
        }
    }
}
```

上面是local算子forward的核心代码，有非常多层的for循环套娃，因为是一个通用的local算子，因此代码里还考虑了$stride、padding$，还有一个比较独特的需求，就是代码中的$is\_xcorr$，因为输入有可能存储方式是倒着存储的。多层的for循环不仅会经常打断流水线的执行，并且经常会影响代码的局部性，从而严重影响程序的性能。很明显，这里存在着非常多的优化空间。

### 3.local算子的加速方案（一）

对于目前的local算子的forward，上述的naive实现都嵌套了6，7层循环，对缓冲的命中非常不友好，参考对卷积常见的优化，im2col后gemm，通过重新排布输入 `tensor`和 `kernel`的数据组织，从而将卷积运算变成了矩阵乘法运算，极大加速卷积的速度。我们也可以采用类似的方案进行实现。

**输入tensor预处理**

如下图所示，首先将 ` input` 进行 `im2col` 操作：

![1627721333195](picture/1627721333195.png)

因为 local kernel 有非常多的参数，因此我们必须考虑所有参数进行矩阵的生成，local kernel 的参数有：`PH,PW,FH,FW,SH,SW` ，这些参数分别代表 Ｈ方向的填充，Ｗ方向的填充，滤波器的尺寸，和滤波器滑动的步长。设输入的 `tensor`的 `shape`为：$[n,c_1,h_1,w_1]$，输出的 `tensor`的 `shape`为：$[n,c_2,h_2,w_2]$，则有：

$$
h_2 = \frac{h_1 + 2 * PH - FH}{SH}　＋ 1 \\
w_2 = \frac{w_1 + 2 * PW - FW}{SW} + 1
$$

由上图可知，我们的输入 `tensor`经过 `im2col`之后，宽度变成了 $c_1*FH*FW$，而我们的高度变成了$h_2*w_2$。

我们将卷积核 `kernel`组织成以下 `shape`，而不再是维度多达6个的多维 `tensor`：

![1627722351963](picture/1627722351963.png)

然后进行矩阵点乘再加和的操作，这便是forward操作，比之前有着更好缓存命中率：

![1627722479276](picture/1627722479276.png)

上述的数据组织形式虽然较之前而言，内存利用效率更高。但根据我们对 `local`算子的观察，变换后的输入 `tensor`的维度为：$(h_2*w_2,c_1*FH*FW)$，很明显，在使用中，变换后的 `tensor`往往是一个在$w$维度上非常窄的长条矩阵，因为后续要进行$sum$操作，因此这种数据组织形式对后续代码进行向量化非常不利，也不利于并行性，因此，我们将数据进行重新组织。我们对两个矩阵都做一个转置。

输入 `tensor`：

![1627724111463](picture/1627724111463.png)

`kernel`：

![1627724155021](picture/1627724155021.png)

因此我们的前向传播变为：

![1627724327473](picture/1627724327473.png)

只讨论一个batch的情况（其他batch可以类推），设最开始的输入 `tensor`为$x$，经过 `im2col`后变成$xi$，`kernel`为$w$，输出为$y$，$xi_i$表示第$i$行$xi$，$w_i$表示第$i$行$w$，$y_i$表示$y$行向量的第$i$个元素。

对于 `local`的 `forward`而言，前向传播公式为：

$$
y = \sum_{i=0}^{c_1*FH*FW}xi_i \odot w_i
$$

这里就存在着非常多的并行可能，比如在向量点乘的时候，比如在向量加和的时候。并且由于我们上述的数据组织方式，前向传播有着最理想的空间局部性，能够尽最大可能利用访存。

而且，编写代码非常简单方便，因为对于简单的点乘累加，只需要在编译选项上加上：

```sh
-Ofast -mavx2 -mfma
```

既能够自动生成采用simd技术优化后的代码，并且性能接近自己编写（虽然但是，笔者还是自己编写了一版，其实和开选项的差异不是很大）。

### 4.local算子的加速方案（二）

第一个方案其实算是接近完美的方案，因为我们改变了filter的数据排布，使得整个local的forward能够以点乘加和的方式进行，但是当我询问导师时才知道，我们不能够改变tensor的数据布局，除非我们reshape。但可想而知，reshape filter的代价有多么的大。因此我们不可能做 reshape filter，filter的数据排布如图所示：![1628926771949](picture/1628926771949.png)

因此，我只能想其他办法了。为此我编写了 `MachineBenchmarker`类，用于测试x86机器的性能，执行

```sh
./test/LZ-ml-engine-test --gtest_filter="BENCHMARKER.MachineBenchmarker"
```

测试出来的机器性能如下：

![benchmark_machine](picture/benchmarker_machine.png)

接着我又搭建了一个能够测试各种算子的Benchmark框架，对naive实现的算子进行测试：

![performance_local_naive](picture/local_naive.png)

由上图发现：

naive的计算吞吐和内存吞吐都非常小，很明显，因为local算子是一个IO任务，但很明显该任务死在了IO上。也就是说TLB和Cache的缺失率非常高，因此，我们需要不仅需要考虑尽可能利用Cache，并且最重要的时要减少TLB地缺失率，这个是最为重要的！要知道，上面我们测试之后发现计算峰值可以达到100GFlops，而这里只有1GFlops。

考虑到输入有 `x` 和 `filter`，他们的维度分别是：`N*IC*IH*IW` 和 `OH * OW * IC * FH * FW * OC`，而 `y` 的维度只有 `N*OC*OH*OW`，因此，很明显我们最主要的是减少  `filter` 的 `TLB` 缺失率。增大 `filter`的 Cache 局部性。同时也得考虑其他两个Tensor的局部性，因此，这里的 x im2col 的形式不再是方案一的数据布局，而是布局成下图的形式：

![im2colNN](picture/1627722351963.png)

这样就可以和在计算过程中增大x的TLB命中率，补上filter的图：

![filter_shape](picture/1628926771949.png)

相当于我们每次只计算一行的数据，增强空间局部性。具体的计算在下节介绍。

### 5.local算子的加速方案（三）

上面的几种方案，im2col 和 矩阵卷积是分开来做的，因此空间复杂度非常大（IC * OC * OH * OW * FW * FH），空间复杂度大，不利于节约内存资源，并且会增加运行时间，这是因为访问内存出现缺页的次数增大的缘故。因此，方案三的目的是为了节约内存的同时减少运行时间，因此：

笔者考虑将im2col和点乘结合起来一起实现，每次im2col完一个patch时，即刻做卷积操作。考虑到向量化能提高运行速度，因此我们还考虑每次im2col完4个patch，2个patch后做卷积。这样编译器便能显式的进行向量优化，提高运行速度。此时算法的所需要的workspace为：（IC * FH * FW * 4）。所以在算法执行过程中，内存缺页次数大幅下降，进一步提供了执行速率：

![1630893623786](picture/1630893623786.png)

根据上图可以看到，上述无论是im2col操作还是卷积操作，都可以并行处理，因此我在代码里都显式的写了对应的代码展开指令，这样方便编译器做优化的时候，优化成对应架构的向量指令（我测过，自己写的simd和优化出来的区别不大），我也测过展开成8，但是那样效果不好，这应该是Cache缺失率导致的，因为设置为4次，可以预加载指令进cache，这也是需要权衡的地方。

### 6.local算子实现性能测试结果


对相同的数据，这里采用 `x=[16,16,300,300],filter=[OH,OW,16,2,2,16]`，其中 `PW=PH=1,SW=SH=1,FW=FH=2`。计算后 `OH=301,OW=301`。对于这个样例，naive的性能测试结果如下：

![local_naive](picture/local_naive.png)

#### 6.1 方案一测试效果

方案一性能测试结果如下：

![performance_result](picture/local_OPT.png)

很明显的看到，方案一的实现，和naive版本相差了11倍的速度，但因为导师说不能改变filter的数据排布，因此这个方案作废（其实这个在部署的时候只要reshape filter 一次，后面的加速效果都能爆增，可惜Megengine没有这个接口，希望Megengine后续补上，这个方案应该是最佳方案了（时间复杂度））。

从这里可以看到，方案一应该是最佳方案，但是迫于不能改变filter的数据排布，所以该方案作废，但该方案的效果却能给我们提示，那就是由于有着im2col的存在，**local实现的效果最高也就只能达到0.5GB/s**。因此，我们后续的目的就是**在不改变filter的数据排布的情况下不断接近方案一的效果**。

因此，下面两个方案，都是以不能改变filter的数据排布为前提。

#### 6.2 方案二测试效果

第二种方案，因为实现的不一样依旧会产生巨大的差异：

假如我们优先考虑 dst 的局部性，那么filter和src的局部性会受到影响，加上除法指令需要更多的周期数，实现后比naive算法更加慢。。。

![case1](picture/case1.png)

然后我们换种方式，把除法指令砍掉，并且留下了更简单的for循环，利于Ofast优化成simd指令代码，发现提高了速度，这下比naive快了，但这还不够。

![case2](picture/case2.png)

这下我们主要考虑filter和src的TLB缺失率问题，因为按照case2的写法，filter和src的TLB缺失率是读一遍filter和src的OC倍，因此我们优先考虑filter和src的TLB缺失率问题，争取他们都被顺序读一次。

![case3](picture/case3.png)

这时候效率已经有了明显的提高了，因此在这个基础上我们再考虑 dst 的TLB缺失率问题，因为按照上面的写法，dst是跨通道数来写的，会产生比较严重的Cache缺失和TLB缺失问题，因此这里我们手动做循环展开，使得一次写入dst的数据变成4个（8个测过，不行）：

![case4](picture/case4.png)

这下提高的非常明显。

方案二的测试效果如下：

![local_opt1](picture/local_OPT1.png)

优化后的效果是naive的大致5倍速度，但是该算法的空间复杂度依然很大。

#### 6.3 方案三测试效果

方案三在方案二的基础上，进一步减少了空间复杂度。前几个方案，都是将im2col和卷积分开进行，而方案三对这两个操作进行融合，在每次im2col完4个patch之后立马做卷积，性能测试结果如下：

![local_OPT2](picture/local_OPT2.png)

可以看到，最终的方案比原先naive的实现提高了大约7倍的速度。

### 7. 整合到Megengine代码中

我已经通过github提交了PR到Megengine中，调用Megengine的benchmark工具，测试得到：

![local_megengine](picture/local.png)

通过Bandwidth可以看出，优化后的local算子比naive实现提高了整整6，7倍左右。但是因为有必不可少的imcol操作的存在，这个速度几乎已经是极限，再往上提高需要对im2col有所突破，但目前并没有找到什么好的方案。

### 8. 总结

在优化naive实现的过程中，通过分析发现，该任务是一个IO密集型任务，因为几乎每个数据都只会被读一次，运算一次。而且输入tensor需要做im2col处理，并且filter的数据布局也非常不友好。因此在开始的时候，我们预设filter的布局是由我们自己决定的。我们对输入做了一个im2col处理，接着便是两个矩阵的点乘累加操作（已经达到理论上最优，毕竟简单）。我发现最好也就只能加速11倍左右，9s的naive被优化到了0.9s。因此，这是我们能达到的极限速度，除此之外很难有突破，因为local的内存读写瓶颈被im2col卡住了。

接着我们在filter不改变数据布局的情况下做优化，因此filter的读数据不能够连续进行。在权衡了filter,src,dst之间的内存读写开销后，我敲定了方案二，后面又因为考虑到空间复杂度，有了方案三。

在测试的全程，因为需要显示每个算子的性能，我还编写了针对算子的benchmark，并同时对x86机器下的各向量指令和带宽进行了测试（虽然后续作用不大）。

**代码已经提交到**[Megengine](https://github.com/MegEngine/MegEngine/pull/207)（点击跳转）**中**。