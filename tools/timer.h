#pragma once
#include <chrono>
#include <ctime>
#include <time.h>

// a tool to count time
class Timer {
public:
  Timer(){};
  ~Timer(){};
  void reset() { m_startTime = std::chrono::high_resolution_clock::now(); }
  double get_secs() const {
    auto endTime = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(endTime -
                                                                m_startTime)
               .count() *
           1e-9;
  }
  double get_nsecs() const { return get_secs() * 1e9; }
  double get_msecs() const { return get_secs() * 1e3; }

private:
  std::chrono::high_resolution_clock::time_point m_startTime;
};