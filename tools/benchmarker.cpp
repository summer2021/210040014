// clang-format off
#include "benchmarker.h"
#include "timer.h"
#include <cstdint>
#include <immintrin.h>
#include <iostream>
#include <memory>
#include <string.h>
#include <unistd.h>
#include <unordered_map>
#include <xmmintrin.h>
#include <sys/sysctl.h>
// clang-format on
#ifdef _WIN32
#include <intrin.h>
#endif

// TODO: need to modifty
using namespace std;

namespace {
bool bit(unsigned x, unsigned y) { return (x >> y) & 1U; }
/******************** detect  ************************/
bool getCpuIdInfo(uint32_t &eax, uint32_t &ebx, uint32_t &ecx, uint32_t &edx) {
#if defined(_WIN32)
  int cpuInfo[4];
  __cpuid(cpuInfo, 7);
  eax = cpuInfo[0];
  ebx = cpuInfo[1];
  ecx = cpuInfo[2];
  edx = cpuInfo[3];
#else
  asm volatile("cpuid\n"
               : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx)
               : "a"(7), "c"(0)
               : "cc");
#endif
  return true;
}

bool isOsSupport(void) {
  uint32_t eax, edx;
  asm volatile("xgetbv" : "=a"(eax), "=d"(edx) : "c"(0));
  return (eax & 6) == 6;
}

using funcType = function<uint32_t(uint32_t)>;
static auto throughputFuncMap = unordered_map<string, funcType>();
static auto latencyFuncMap = unordered_map<string, funcType>();

size_t addThroughputFuncMap(string name, funcType func) {
  throughputFuncMap[name] = func;
  return 0;
}

size_t addlatencyFuncMap(string name, funcType func) {
  latencyFuncMap[name] = func;
  return 0;
}

#define ATTRIBUTE_TARGET(simd) __attribute__((target(simd)))
#define eor(i) "vxorps %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
// clang-format off
#define THROUGHPUT(cb, func, simd)                                 \
    ATTRIBUTE_TARGET(#simd)                                        \
    static uint32_t func##_ ##simd ##_throughput(uint32_t runTimes) {\
        asm volatile(                                              \
        UNROLL_CODE(eor, 10)                                       \
        "movl %[RUNS], %%eax \n"                                   \
        "1:\n"                                                     \
        UNROLL_CODE(cb, 10)                                        \
        "sub  $0x01, %%eax\n"                                      \
        "jne 1b \n"                                                \
        :                                                          \
        :[RUNS] "r"(runTimes * 100)                                \
        : "%ymm0", "%ymm1", "%ymm2", "%ymm3", "%ymm4", "%ymm5",    \
           "%ymm6", "%ymm7", "%ymm8", "%ymm9", "%eax", "cc");      \
        return runTimes * 100 * 10;                                \
    }                                                              \
    static auto func##_##simd##_throughput_ret =                      \
    addThroughputFuncMap(#simd "_" #func,func##_ ##simd ##_throughput);                                                             

#define LATENCY(cb, func, simd)                                \
    ATTRIBUTE_TARGET(#simd)                                    \
    static uint32_t func##_##simd##_latency(uint32_t runTimes) { \
        asm volatile(                                          \
        "vxorpd %%ymm0, %%ymm0, %%ymm0\n"                      \
        "movl %[RUNS], %%eax \n"                               \
        "1:\n"                                                 \
        UNROLL_CODE(cb, 10)                                    \
        "sub  $0x01, %%eax\n"                                  \
        "jne 1b \n"                                            \
        :                                                      \
        :[RUNS] "r"(runTimes * 100)                            \
        : "%ymm0", "%eax", "cc");                              \
        return runTimes * 100 * 10;                            \
    }                                                          \
    static auto func##_##simd##_latency_ret =                     \
    addlatencyFuncMap(#simd "_" #func,func##_##simd##_latency);

#define cb(i) "vfmadd132ps %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vfmadd132ps, avx2)
#undef cb
#define cb(i) "vfmadd132ps %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vfmadd132ps, avx2)
#undef cb

#define cb(i) "vfmadd132pd %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vfmadd132pd, avx2)
#undef cb
#define cb(i) "vfmadd132pd %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vfmadd132pd, avx2)
#undef cb

#define cb(i) "vpmaddwd %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vpmaddwd, avx2)
#undef cb
#define cb(i) "vpmaddwd %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vpmaddwd, avx2)
#undef cb

#define cb(i) "vpaddd %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vpaddd, avx2)
#undef cb
#define cb(i) "vpaddd %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vpaddd, avx2)
#undef cb

#define cb(i) "vpacksswb %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vpacksswb, avx2)
#undef cb
#define cb(i) "vpacksswb %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vpacksswb, avx2)
#undef cb

#define cb(i) "vpackssdw %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vpackssdw, avx2)
#undef cb
#define cb(i) "vpackssdw %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vpackssdw, avx2)
#undef cb

#define cb(i) "vpand %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vpand, avx2)
#undef cb
#define cb(i) "vpand %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vpand, avx2)
#undef cb

#define cb(i) "vunpckhps %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vunpckhps, avx2)
#undef cb

#define cb(i) "vunpckhps %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vunpckhps, avx2)
#undef cb

#define cb(i) "vunpcklps %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vunpcklps, avx2)
#undef cb
#define cb(i) "vunpcklps %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vunpcklps, avx2)
#undef cb

// clang-format off

ATTRIBUTE_TARGET("avx2")
static uint32_t vbroadcastss_avx2_throughput(uint32_t runTimes) {
  float acc = 1.0f;
  float *ptr = &acc;
#define cb(i) "vbroadcastss %[ptr], %%ymm" #i "\n"
  asm volatile(
      UNROLL_CODE(eor, 10) 
      "movl %[RUNS], %%eax \n"
      "1:\n" 
      UNROLL_CODE(cb, 10) 
      "sub  $0x01, %%eax\n"
      "jne 1b \n"
      :
      : [RUNS] "r"(runTimes * 100), [ptr] "g"(ptr)
      : "%ymm0", "%ymm1", "%ymm2", "%ymm3", "%ymm4", "%ymm5", "%ymm6", "%ymm7",
        "%ymm8", "%ymm9", "%eax", "cc");
#undef cb
  return runTimes * 100 * 10;
}
static auto vbroadcastss_avx2_throughput_ret =
    addThroughputFuncMap("avx2_vbroadcastss", vbroadcastss_avx2_throughput);

ATTRIBUTE_TARGET("avx2")
static uint32_t vbroadcastss_avx2_latency(uint32_t runTimes) {
  float acc = 1.0f;
  const float *ptr = &acc;
#define cb(i) "vbroadcastss %[ptr] , %%ymm0\n"
  asm volatile("vxorpd %%ymm0, %%ymm0, %%ymm0\n"
               "movl %[RUNS], %%eax \n"
               "1:\n" 
               UNROLL_CODE(cb, 10) 
               "sub  $0x01, %%eax\n"
               "jne 1b \n"
               :
               : [RUNS] "r"(runTimes * 100), [ptr] "g"(ptr)
               : "%ymm0", "%eax", "cc");
#undef cb
  return runTimes * 100 * 10;
}
static auto vbroadcastss_avx2_latency_ret =
    addlatencyFuncMap("avx2_vbroadcastss", vbroadcastss_avx2_latency);

// clang-format on
#define cb(i)                                                                  \
  "vpmaddwd %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"                           \
  "vpaddd %%ymm" #i ", %%ymm" #i ", %%ymm" #i "\n"
THROUGHPUT(cb, vpmaddwd_vpaddd, avx2)
#undef cb
// clang-format on
#define cb(i)                                                                  \
  "vpmaddwd %%ymm0, %%ymm0, %%ymm0\n"                                          \
  "vpaddd %%ymm0, %%ymm0, %%ymm0\n"
LATENCY(cb, vpmaddwd_vpaddd, avx2)
#undef cb

#define cb(i) "vpmaddwd %%zmm" #i ", %%zmm" #i ", %%zmm" #i "\n"
THROUGHPUT(cb, vpmaddwd_512, avx512bw)
#undef cb
#define cb(i) "vpmaddwd %%zmm0, %%zmm0, %%zmm0\n"
LATENCY(cb, vpmaddwd_512, avx512bw)
#undef cb

#define cb(i) "vpaddd %%zmm" #i ", %%zmm" #i ", %%zmm" #i "\n"
THROUGHPUT(cb, vpaddd_512, avx512bw)
#undef cb
#define cb(i) "vpaddd %%zmm0, %%zmm0, %%zmm0\n"
LATENCY(cb, vpaddd_512, avx512f)
#undef cb

#define cb(i) "vfmadd132ps %%zmm" #i ", %%zmm" #i ", %%zmm" #i "\n"
THROUGHPUT(cb, vfmadd132ps_512, avx512f)
#undef cb
#define cb(i) "vfmadd132ps %%zmm0, %%zmm0, %%zmm0\n"
LATENCY(cb, vfmadd132ps_512, avx512f)
#undef cb

#if __AVX512VNNI__

#define cb(i) "vpdpbusd %%zmm" #i ", %%zmm" #i ", %%zmm" #i "\n"
THROUGHPUT(cb, vpdpbusd, avx512vnni)
hardwareFuncsType
#undef cb
#define cb(i) "vpdpbusd %%zmm0, %%zmm0, %%zmm0\n"
    LATENCY(cb, vpdpbusd, avx512vnni)
#undef cb

#endif

#undef eor
#undef THROUGHPUT
#undef LATENCY

#define eor(i) "xorps %%xmm" #i ", %%xmm" #i "\n"

// clang-format off
#define THROUGHPUT(cb, func, simd)                                 \
    ATTRIBUTE_TARGET(simd)                                         \
    static uint32_t func##_sse##_throughput(uint32_t runTimes) {         \
        asm volatile(                                              \
        UNROLL_CODE(eor, 10)                                       \
        "movl %[RUNS], %%eax \n"                                   \
        "1:\n"                                                     \
        UNROLL_CODE(cb, 10)                                       \
        "sub  $0x01, %%eax\n"                                      \
        "jne 1b \n"                                                \
        :                                                          \
        :[RUNS] "r"(runTimes * 100)                                \
        : "%xmm0", "%xmm1", "%xmm2", "%xmm3", "%xmm4", "%xmm5",    \
           "%xmm6", "%xmm7", "%xmm8", "%xmm9", "%eax", "cc");      \
        return runTimes * 100 * 10;                                \
    }                                                              \
    static auto func##_sse##_throughput_ret =                            \
    addThroughputFuncMap(simd "_" #func,func##_sse##_throughput);


#define LATENCY(cb, func, simd)                                \
    ATTRIBUTE_TARGET(simd)                                     \
    static uint32_t func##_sse##_latency(uint32_t runTimes) {        \
        asm volatile(                                          \
        "xorpd %%xmm0, %%xmm0\n"                               \
        "movl %[RUNS], %%eax \n"                               \
        "1:\n"                                                 \
        UNROLL_CODE(cb, 10)                                    \
        "sub  $0x01, %%eax\n"                                  \
        "jne 1b \n"                                            \
        :                                                      \
        :[RUNS] "r"(runTimes * 100)                            \
        : "%xmm0", "%eax", "cc");                              \
        return runTimes * 100 * 10;                            \
    }                                                          \
    static auto func##_sse##_latency_ret =                           \
    addlatencyFuncMap(simd "_" #func,func##_sse##_latency);

// clang-format off
#define cb(i) "mulps %%xmm" #i ", %%xmm" #i "\n"
THROUGHPUT(cb, mulps, "sse2")
#undef cb
#define cb(i) "mulps %%xmm0, %%xmm0\n"
LATENCY(cb, mulps, "sse2")
#undef cb

#define cb(i) "mulpd %%xmm" #i ", %%xmm" #i "\n"
THROUGHPUT(cb, mulpd, "sse2")
#undef cb
#define cb(i) "mulpd %%xmm0, %%xmm0\n"
LATENCY(cb, mulpd, "sse2")
#undef cb

#define cb(i) "vfnmadd132ps %%xmm" #i ", %%xmm" #i ", %%xmm" #i "\n"
THROUGHPUT(cb, vfmadd132ps, "fma")
#undef cb
#define cb(i) "vfmadd132ps %%xmm0, %%xmm0, %%xmm0\n"
LATENCY(cb, vfmadd132ps, "fma")
#undef cb
#define cb(i)                                          \
    "vpmaddwd %%xmm" #i ", %%xmm" #i ", %%xmm" #i "\n" \
    "vpaddd %%xmm" #i ", %%xmm" #i ", %%xmm" #i "\n"
THROUGHPUT(cb, vpmaddwd_vpaddd, "sse2")
#undef cb
#define cb(i)                           \
    "vpmaddwd %%xmm0, %%xmm0, %%xmm0\n" \
    "vpaddd %%xmm0, %%xmm0, %%xmm0\n"
LATENCY(cb, vpmaddwd_vpaddd, "sse2")
#undef cb
#undef eor
#undef THROUGHPUT
#undef LATENCY
// clang-format on
} // namespace

MachineBenchmarker::MachineBenchmarker() {
  m_supportList.resize((size_t)SIMDTYPE::SIMDTYPE_NUM);
  std::fill(m_supportList.begin(), m_supportList.end(), false);
  detectSupportedHardware();
}

void MachineBenchmarker::printMachineFullMemorySpeed() {
  constexpr size_t warmupNum = 5;
  constexpr size_t runNum = 40;
  constexpr size_t arrLen = 1024 * 1024 * 100;
  auto timer = Timer();
  std::unique_ptr<uint8_t[]> src{new uint8_t[arrLen]};
  std::unique_ptr<uint8_t[]> dst{new uint8_t[arrLen]};
  volatile uint8_t res = 0;
  for (size_t i = 0; i < warmupNum; i++) {
    memcpy(dst.get(), src.get(), arrLen);
    res += dst[0];
  }
  timer.reset();
  for (size_t i = 0; i < runNum; i++) {
    memcpy(dst.get(), src.get(), arrLen);
    res += dst[0];
  }
  auto secs = timer.get_secs();
  double eachRunTime = secs / runNum;
  double GBByteNum = (1024.0 * 1024.0 * 1024.0);
  double speed = ((2 * arrLen) / GBByteNum) / eachRunTime;
  cout << "the target time is : " << speed << " GB/s" << endl;
}

/*********
_SC_CHILD_MAX：       每个user可同时运行的最大进程数
_SC_HOST_NAME_MAX：   hostname最大长度，需小于_POSIX_HOST_NAME_MAX (255)
_SC_OPEN_MAX：        一个进程可同时打开的文件最大数
_SC_PAGESIZE：        一个page的大小，单位byte
_SC_PHYS_PAGES：      物理内存总page数
_SC_AVPHYS_PAGES：    当前可获得的物理内存page数
_SC_NPROCESSORS_CONF：配置的处理器个数
_SC_NPROCESSORS_ONLN：当前可获得的处理器个数
_SC_CLK_TCK：         每秒对应的时钟tick数
**********/
void MachineBenchmarker::printCoreNum() {
  size_t coreNum = sysconf(_SC_NPROCESSORS_ONLN);
  cout << "the computer has " << coreNum << " cores" << endl;
}

bool MachineBenchmarker::isSupported(SIMDTYPE type) {
  size_t type2index = (size_t)type;
  if (type >= SIMDTYPE::SIMDTYPE_NUM)
    return false;
  return m_supportList[type2index];
}

void MachineBenchmarker::setCpuAffinity(size_t cpuid) {
  // cpu_set_t cst;
  // CPU_ZERO(&cst);
  // CPU_SET(cpuid, &cst);
  // TODO: need to support mac
  // sched_setaffinity(0, sizeof(cst), &cst);
}

void MachineBenchmarker::detectSupportedHardware() {
  uint32_t eax, ebx, ecx, edx;
  if (!getCpuIdInfo(eax, ebx, ecx, edx))
    return;
  auto osSupportFlag = isOsSupport();
  // check AVX2
  if (bit(ebx, 3) && bit(ebx, 5) && bit(ebx, 8) && osSupportFlag)
    m_supportList[(size_t)SIMDTYPE::AVX2] = true;

  // check AVX512
  if (bit(ebx, 16) && osSupportFlag)
    m_supportList[(size_t)SIMDTYPE::AVX512] = true;

  // check VNNI
  if (bit(ebx, 16) && bit(ebx, 17) && bit(ebx, 30) && bit(ebx, 31) &&
      bit(ecx, 11) && osSupportFlag)
    m_supportList[(size_t)SIMDTYPE::VNNI] = true;

#if defined(_WIN32)
  int cpuInfo[4];
  __cpuid(cpuInfo, 1);
  eax = cpuInfo[0];
  ebx = cpuInfo[1];
  ecx = cpuInfo[2];
  edx = cpuInfo[3];
#else
  asm volatile("cpuid\n"
               : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx)
               : "a"(1)
               : "cc");
#endif
  // check avx
  if (bit(ecx, 27) && bit(ecx, 28) && osSupportFlag)
    m_supportList[(size_t)SIMDTYPE::AVX] = true;
  // check fma
  if (bit(ecx, 27) && bit(ecx, 12) && osSupportFlag)
    m_supportList[(size_t)SIMDTYPE::FMA] = true;
  // check SSE
  if (bit(edx, 25))
    m_supportList[(size_t)SIMDTYPE::SSE] = true;
  // check SSE2
  if (bit(edx, 26))
    m_supportList[(size_t)SIMDTYPE::SSE2] = true;
  // check SSE3
  if (bit(edx, 0))
    m_supportList[(size_t)SIMDTYPE::SSE3] = true;
  // check SSE4_1
  if (bit(edx, 19))
    m_supportList[(size_t)SIMDTYPE::SSE4_1] = true;
  // check SSE4_2
  if (bit(edx, 20))
    m_supportList[(size_t)SIMDTYPE::SSE4_2] = true;
}

void MachineBenchmarker::printSupportedHardware(void) {
  vector<string> harewareName = {"SSE", "SSE2", "SSE3", "SSE4_1", "SSE4_2",
                                 "AVX", "AVX2", "FMA",  "AVX512", "VNNI"};
  for (size_t index = 0; index < m_supportList.size() - 1; index++) {

    if (m_supportList[index] == false)
      cout << "the computer not support " << harewareName[index] << "." << endl;
    else
      cout << "the computer support " << harewareName[index] << "." << endl;
    ;
  }
}

/****************** test the speed of hardware **********************/

void MachineBenchmarker::printHardwareSpeed(void) {
  uint32_t runNum = 800000;
  auto getMapFunc = [](unordered_map<string, funcType> &funcMap,
                       string funcName) -> funcType {
    auto funcIter = funcMap.find(funcName);
    if (funcIter == funcMap.end())
      return nullptr;
    return funcIter->second;
  };
  auto printFunc = [&](string hardwareName1, string hardwareName2,
                       string funcName, size_t simdLen) {
    string throughFuncName = hardwareName1 + "_" + funcName;
    string latencyFuncName = hardwareName2 + "_" + funcName;
    auto throughputFunc = getMapFunc(throughputFuncMap, throughFuncName);
    auto latencyFunc = getMapFunc(latencyFuncMap, latencyFuncName);
    if (throughputFunc == nullptr || latencyFunc == nullptr)
      return;
    for (size_t i = 0; i < 10; i++) {
      throughputFunc(runNum);
    }
    Timer timer;
    timer.reset();
    auto allRunTimes = throughputFunc(runNum);
    auto throuphputTime = timer.get_nsecs() / allRunTimes;
    timer.reset();
    allRunTimes = latencyFunc(runNum);
    auto latencyTime = timer.get_nsecs() / allRunTimes;
    cout << throughFuncName << " throughput: " << throuphputTime << " ns "
         << 1.f / throuphputTime * simdLen << " GFlops latency: " << latencyTime
         << " ns" << endl;
  };

  if (isSupported(SIMDTYPE::FMA) && isSupported(SIMDTYPE::AVX)) {
    printFunc("avx2", "avx2", "vfmadd132ps", 8 * 2);
    printFunc("avx2", "avx2", "vfmadd132pd", 4 * 2);
  }
  if (isSupported(SIMDTYPE::AVX2)) {
    printFunc("avx2", "avx2", "vpmaddwd", 16 + 8);
    printFunc("avx2", "avx2", "vpand", 8);
    printFunc("avx2", "avx2", "vpaddd", 8);
    printFunc("avx2", "avx2", "vpmaddwd_vpaddd", 16 + 8 + 8);
    printFunc("avx2", "avx2", "vpackssdw", 16);
    printFunc("avx2", "avx2", "vpacksswb", 32);
    printFunc("avx2", "avx2", "vunpckhps", 8);
    printFunc("avx2", "avx2", "vunpcklps", 8);
    printFunc("avx2", "avx2", "vbroadcastss", 8);
  }
  if (isSupported(SIMDTYPE::AVX512)) {
    printFunc("avx512bw", "avx512bw", "vpmaddwd_512", 32 + 16);
    printFunc("avx512bw", "avx512f", "vpaddd_512", 16);
    printFunc("avx512f", "avx512f", "vfmadd132ps_512", 16 * 2);
  }
  if (isSupported(SIMDTYPE::VNNI)) {
    printFunc("avx512vnni", "avx512vnni", "vpdpbusd", 112);
  }
  if (isSupported(SIMDTYPE::SSE2)) {
    printFunc("sse2", "sse2", "mulps", 4 * 2);
    printFunc("sse2", "sse2", "mulpd", 2 * 2);
    printFunc("sse2", "sse2", "vpmaddwd_vpaddd", 8 + 4 + 4);
  }
  if (isSupported(SIMDTYPE::FMA)) {
    printFunc("fma", "fma", "vfmadd132ps", 4 * 2);
  }
}