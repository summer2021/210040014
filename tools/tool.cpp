#include "tool.h"
#include <iostream>
#include <math.h>

void TensorShower::show(Tensor &tensor) { show(tensor.layout); }

void TensorShower::show(TensorLayout &layout) {
  std::cout << "**********************" << std::endl;
  // show the shape
  auto shape = layout.shape;
  std::cout << "the tensor's shape is [";
  auto size = shape.size();
  if (shape.empty()) {
    std::cout << "]" << std::endl << "**********************" << std::endl;
    return;
  }
  for (size_t i = 0; i < size - 1; i++) {
    std::cout << shape[i] << " ,";
  }
  std::cout << shape[size - 1] << "]";
  // show the real content
  if (size == 1) {
    // vector
  } else if (size == 2) {
    // matrix
  } else {
    // for tensor
  }
  std::cout << std::endl << "**********************" << std::endl;
}

bool equal_tensor(Tensor &a, Tensor &b) {
  auto a_layout = a.layout;
  auto b_layout = b.layout;
  auto a_src = (float *)a.raw_ptr;
  auto b_src = (float *)b.raw_ptr;
  if (a_layout.size != b_layout.size)
    return false;
  for (size_t i = 0; i < a_layout.size; i++)
    if (abs(a_src[i] - b_src[i]) > 1e-4)
      return false;
  return true;
}

void TensorShower::print(Tensor &tensor) {
  auto ptr = (float *)tensor.raw_ptr;
  if (tensor.layout.size == 2) {
    auto M = tensor.layout.shape[0];
    auto N = tensor.layout.shape[1];
    for (size_t m = 0; m < M; m++) {
      for (size_t n = 0; n < N; n++) {
        printf("%.2f ", ptr[m * N + n]);
      }
      printf("\n");
    }
    return;
  }
  for (size_t m = 0; m < tensor.layout.size; m++) {
    printf("%.2f ", ptr[m]);
  }
}

bool equal_tensor_t(Tensor &a, Tensor &b) {
  auto a_layout = a.layout;
  auto b_layout = b.layout;
  auto A = (float *)a.raw_ptr;
  auto B = (float *)b.raw_ptr;
  if (a_layout.size != b_layout.size || b_layout.shape.size() != 2 ||
      a_layout.shape.size() != 2)
    return false;
  if (a_layout.shape[0] != b_layout.shape[1] ||
      a_layout.shape[1] != b_layout.shape[0])
    return false;
  auto M = a_layout.shape[0];
  auto N = a_layout.shape[1];
  for (size_t m = 0; m < M; m++) {
    for (size_t n = 0; n < N; n++) {
      if (abs(B[n * M + m] - A[m * N + n]) > 1e-4)
        return false;
    }
  }
  return true;
}