#pragma once
#include "tensorBase.h"
#include <cstdio>

class TensorShower {
public:
  TensorShower(){};
  ~TensorShower(){};
  void show(Tensor &tensor);
  void show(TensorLayout &layout);
  void print(Tensor &tensor);
};

bool equal_tensor(Tensor &a, Tensor &b);
bool equal_tensor_t(Tensor &a, Tensor &b);
