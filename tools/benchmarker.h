#pragma once
#include "macro.h"
#include "tensorBase.h"
#include "timer.h"
#include "tool.h"
#include <functional>
#include <iostream>
#include <vector>
enum class SIMDTYPE {
  SSE,
  SSE2,
  SSE3,
  SSE4_1,
  SSE4_2,
  AVX,
  AVX2,
  FMA,
  AVX512,
  VNNI,
  SIMDTYPE_NUM //! total number of SIMD types; used for testing
};

// TODO: Arity为其他的情况
template <typename Operator, size_t Arity> struct ExeProxy;

#define cb(i) tensorLayoutVec[i],

#define EXEPROXY_DELARE(i1, i)                                                 \
  template <typename Operator> struct ExeProxy<Operator, i> {                  \
    void operator()(size_t testTime, Operator *opr,                            \
                    TensorLayoutVec tensorLayoutVec) {                         \
      if (tensorLayoutVec.size() != i) {                                       \
        assert(opr->s_inputsNum + opr->s_outputsNum == i,                      \
               "the oprs number is error");                                    \
      }                                                                        \
      opr->exec(UNROLL_CODE(cb, i1) tensorLayoutVec[i - 1]);                   \
    }                                                                          \
  };

//下面的可以作为参考
template <typename Operator> struct ExeProxy<Operator, 2> {
  void operator()(size_t testTime, Operator *opr,
                  TensorLayoutVec tensorLayoutVec) {
    // TODO: 安全性检查
    if (tensorLayoutVec.size() != 2) {
      // assert(opr->s_inputsNum + opr->s_outputsNum == 3,
      //        "the oprs number is error");
    }
    TensorLayout dst({});
    opr->deduce_layout(tensorLayoutVec[0], tensorLayoutVec[1], dst);
    auto workspace_byte =
        opr->get_workspace_in_bytes(tensorLayoutVec[0], tensorLayoutVec[1]);
    auto TotalCalculation =
        opr->getTotalCalculation(tensorLayoutVec[0], tensorLayoutVec[1]);
    auto TotalThroughput =
        opr->getTotalThroughput(tensorLayoutVec[0], tensorLayoutVec[1]);
    // TODO: tensor 的内存管理
    WorkSpace workspace(new uint8_t[workspace_byte], workspace_byte);
    auto x1_ptr = new float[tensorLayoutVec[0].size];
    auto x2_ptr = new float[tensorLayoutVec[1].size];
    auto dst_ptr = new float[dst.size];
    Tensor x1(tensorLayoutVec[0], x1_ptr);
    Tensor x2(tensorLayoutVec[1], x2_ptr);
    Tensor y(dst, dst_ptr);
    x1.randn();
    x2.randn();
    y.set_zero();
    auto timer = Timer();
    timer.reset();
    for (size_t i = 0; i < testTime; i++) {
      opr->exec(x1, x2, y, workspace);
    }
    auto allTime = timer.get_secs() / (float)testTime;
    std::cout << "it use : " << allTime << " s time." << std::endl;
    double compute = TotalCalculation / (1000.0 * 1000.0 * 1000.0) / allTime;
    double throughput = TotalThroughput / (1024.0 * 1024.0 * 1024.0) / allTime;
    std::cout << "compute : " << compute << " Gflops" << std::endl;
    std::cout << "throughput : " << throughput << " GB/s" << std::endl;
    delete[] x1_ptr;
    delete[] x2_ptr;
    delete[] dst_ptr;
  }
};
// EXEPROXY_DELARE(0, 1)
// EXEPROXY_DELARE(1, 2)
// EXEPROXY_DELARE(2, 3)
// EXEPROXY_DELARE(3, 4)
// EXEPROXY_DELARE(4, 5)

#undef cb
#undef EXEPROXY_DELARE

// TODO: to finish Benchmarker
template <typename Operator> class Benchmarker {
  using Param = typename Operator ::Param;
  void addTensorLayout(TensorLayout tensorLayout) {
    m_tensorLayoutVec.push_back(tensorLayout);
  }
  template <typename... Args, typename TensorShapeType>
  void addTensorLayout(TensorShapeType tensorLayout, Args... args) {
    m_tensorLayoutVec.push_back(tensorLayout);
    addTensorLayout(args...);
  }

public:
  Benchmarker() : m_testTime(1000) {
    m_operator = new Operator();
    m_tensorLayoutVec.clear();
  }
  void setTestTime(size_t testTime) { m_testTime = testTime; }
  template <typename... Args> void exec(Args... args) {
    addTensorLayout(args...);
    m_operator->setParam(m_param);
    ExeProxy<Operator, sizeof...(args)>()(m_testTime, m_operator,
                                          m_tensorLayoutVec);
  }

public:
  Param m_param;
  Operator *m_operator;
  TensorLayoutVec m_tensorLayoutVec;

private:
  size_t m_testTime;
};

class MachineBenchmarker {
public:
  MachineBenchmarker();
  void printSupportedHardware(void);
  void printHardwareSpeed(void);
  void printMachineFullMemorySpeed();
  void printCoreNum();
  void setCpuAffinity(size_t cpuid);

private:
  std::vector<bool> m_supportList;
  bool isSupported(SIMDTYPE type);
  void detectSupportedHardware(void);
};