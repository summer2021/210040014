# 运行环境

+ 操作系统：Ubuntu 20 x64
+ 编译器：clang++

# Run

```sh
sh third_party/prepare.sh # git clone 依赖库 gtest
mkdir build & cd build
cmake ..
make 
./test/LZ-ml-engine-test
```

# 其他

具体内容参考 report.md
