#pragma once
#include "tensorBase.h"
#include <string>

class OperatorBase {
public:
  // virtual void exec(Tensor &src, Tensor &filter, Tensor &dst,
  //                   WorkSpace &workspace) = 0;
  // virtual size_t get_workspace_byte(Tensor &src, Tensor &filter) = 0;
  // virtual void set_necessity_param(size_t ph, size_t pw, size_t sh,
  //                                  size_t sw) = 0;
  std::string name;
};