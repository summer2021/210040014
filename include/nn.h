#pragma once
#include "operatorBase.h"
#include "param.h"
namespace lzdnn {
#define DEF_OPR_PARAM(operator)                                                \
public:                                                                        \
  using Param = param::operator;                                               \
  Param &param() { return m_param; }                                           \
  const Param &param() const { return m_param; }                               \
  void setParam(Param param) { m_param = param; }                              \
                                                                               \
protected:                                                                     \
  Param m_param

#define DEF_OPR_IN_OUT(inputsNum, outputsNum)                                  \
  static const size_t s_inputsNum = inputsNum;                                 \
  static const size_t s_outputsNum = outputsNum

#define tensor_in const Tensor &
#define tensor_out Tensor &

/****************** Local ***********************/
class LocalBase : public OperatorBase {
  DEF_OPR_PARAM(Convolution);
};

class LocalForward : public LocalBase {
  DEF_OPR_IN_OUT(2, 1);

public:
  virtual void exec(tensor_in src, tensor_in filter, tensor_out dst,
                    WorkSpace &workspace) = 0;
#define PARSING_PARAM                                                          \
  auto N = src.shape[0];                                                       \
  auto IC = src.shape[1];                                                      \
  auto IH = src.shape[2];                                                      \
  auto IW = src.shape[3];                                                      \
  auto FH = filter.shape[3];                                                   \
  auto FW = filter.shape[4];                                                   \
  auto SH = m_param.sh;                                                        \
  auto SW = m_param.sw;                                                        \
  auto PH = m_param.ph;                                                        \
  auto PW = m_param.pw;                                                        \
  auto OC = filter.shape[5];                                                   \
  auto OH = (IH + 2 * PH - FH) / SH + 1;                                       \
  auto OW = (IW + 2 * PW - FW) / SW + 1;
  void deduce_layout(const TensorLayout &src, const TensorLayout &filter,
                     TensorLayout &dst) {
    PARSING_PARAM
    dst = {N, OC, OH, OW};
  }
  size_t getTotalCalculation(const TensorLayout &src,
                             const TensorLayout &filter) {
    PARSING_PARAM
    return N * OC * OH * OW * IC * FH * FW * 2;
  }
  size_t getTotalThroughput(const TensorLayout &src,
                            const TensorLayout &filter) {
    PARSING_PARAM
    return (src.size + filter.size + N * OC * OH * OW) * sizeof(float);
  }
#undef PARSING_PARAM

  virtual size_t get_workspace_in_bytes(const TensorLayout &src,
                                        const TensorLayout &filter) = 0;

protected:
  // TODO: need check in exec
  bool check_exec(const TensorLayout &src, const TensorLayout &filter,
                  const TensorLayout &dst, size_t workspace_in_bytes) {
    TensorLayout deduce_dst({});
    deduce_layout(src, filter, deduce_dst);
    if (deduce_dst != dst) {
      return false;
    }
    auto deduce_workspace = get_workspace_in_bytes(src, filter);
    if (deduce_workspace > workspace_in_bytes)
      return false;
    return true;
  }
};

class LocalBackwardData : public LocalBase {
  DEF_OPR_IN_OUT(2, 1);

public:
  virtual void exec(tensor_in filter, tensor_in diff, tensor_out grad,
                    WorkSpace &workspace) = 0;
  void deduce_layout(const TensorLayout &filter, const TensorLayout &diff,
                     TensorLayout &grad);
  virtual size_t get_workspace_in_bytes(const TensorLayout &filter,
                                        const TensorLayout &diff) = 0;

protected:
  void check_exec(const TensorLayout &filter, const TensorLayout &diff,
                  const TensorLayout &grad, size_t workspace_in_bytes);
};

class LocalBackwardFilter : public LocalBase {
  DEF_OPR_IN_OUT(2, 1);

public:
  virtual void exec(tensor_in src, tensor_in diff, tensor_out grad,
                    WorkSpace &workspace) = 0;
  void deduce_layout(const TensorLayout &src, const TensorLayout &diff,
                     TensorLayout &grad);
  virtual size_t get_workspace_in_bytes(const TensorLayout &src,
                                        const TensorLayout &diff) = 0;

protected:
  void check_exec(const TensorLayout &src, const TensorLayout &diff,
                  const TensorLayout &grad, size_t workspace_in_bytes);
};

/****************** Conv ***********************/

#undef DEF_OPR_PARAM
#undef DEF_OPR_IN_OUT
} // namespace lzdnn
