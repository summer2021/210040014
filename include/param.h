#include <cstddef>

namespace param {
struct Convolution {
  size_t ph, pw, sh, sw;
};
} // namespace param