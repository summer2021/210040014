#define UNROLL_CODE0(cb, a...)

#define UNROLL_CODE1(cb, a...) cb(0, ##a)
#define UNROLL_CODE2(cb, a...) cb(0, ##a) cb(1, ##a)
#define UNROLL_CODE3(cb, a...)                                                 \
  UNROLL_CODE2(cb, ##a)                                                        \
  cb(2, ##a)
#define UNROLL_CODE4(cb, a...)                                                 \
  UNROLL_CODE3(cb, ##a)                                                        \
  cb(3, ##a)
#define UNROLL_CODE5(cb, a...)                                                 \
  UNROLL_CODE4(cb, ##a)                                                        \
  cb(4, ##a)
#define UNROLL_CODE6(cb, a...)                                                 \
  UNROLL_CODE5(cb, ##a)                                                        \
  cb(5, ##a)
#define UNROLL_CODE7(cb, a...)                                                 \
  UNROLL_CODE6(cb, ##a)                                                        \
  cb(6, ##a)
#define UNROLL_CODE8(cb, a...)                                                 \
  UNROLL_CODE7(cb, ##a)                                                        \
  cb(7, ##a)
#define UNROLL_CODE9(cb, a...)                                                 \
  UNROLL_CODE8(cb, ##a)                                                        \
  cb(8, ##a)
#define UNROLL_CODE10(cb, a...)                                                \
  UNROLL_CODE9(cb, ##a)                                                        \
  cb(9, ##a)
#define UNROLL_CODE11(cb, a...)                                                \
  UNROLL_CODE10(cb, ##a)                                                       \
  cb(10, ##a)
#define UNROLL_CODE12(cb, a...)                                                \
  UNROLL_CODE11(cb, ##a)                                                       \
  cb(11, ##a)
#define UNROLL_CODE13(cb, a...)                                                \
  UNROLL_CODE12(cb, ##a)                                                       \
  cb(12, ##a)
#define UNROLL_CODE14(cb, a...)                                                \
  UNROLL_CODE13(cb, ##a)                                                       \
  cb(13, ##a)
#define UNROLL_CODE15(cb, a...)                                                \
  UNROLL_CODE14(cb, ##a)                                                       \
  cb(14, ##a)
#define UNROLL_CODE16(cb, a...)                                                \
  UNROLL_CODE15(cb, ##a)                                                       \
  cb(15, ##a)
#define UNROLL_CODE(cb, i, a...) UNROLL_CODE##i(cb, ##a)