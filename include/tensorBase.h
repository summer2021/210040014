// 先随便糊一个
#pragma once
#include <cstddef>
#include <random>
#include <vector>

struct WorkSpace {
  ~WorkSpace() {
    if (raw_ptr != nullptr)
      delete[](float *) raw_ptr;
  }
  WorkSpace(void *raw_ptr_, size_t size_) : raw_ptr(raw_ptr_), size(size_) {}
  void *raw_ptr;
  size_t size;
};

struct TensorLayout {
  TensorLayout(std::vector<size_t> shape_) : shape(shape_) {
    if (shape_.size() == 0) {
      size = 0;
      return;
    }
    size = 1;
    for (auto elem : shape_) {
      size *= elem;
    }
  };
  void operator=(std::vector<size_t> shape_) {
    shape = shape_;
    size = 1;
    for (auto elem : shape_) {
      size *= elem;
    }
  }
  bool operator==(const TensorLayout &src) {
    if (size != src.size)
      return false;
    if (shape.size() != src.shape.size())
      return false;
    for (size_t i = 0; i < src.shape.size(); i++) {
      if (shape[i] != src.shape[i])
        return false;
    }
    return true;
  }
  bool operator!=(const TensorLayout &src) { return !operator==(src); }
  std::vector<size_t> shape;
  size_t operator[](size_t i) { return shape[i]; }
  size_t size;
};

struct Tensor {
  Tensor(TensorLayout &shape) : layout(shape) {}
  Tensor(TensorLayout &shape, void *ptr) : layout(shape), raw_ptr(ptr){};
  Tensor(std::vector<size_t> shape, void *ptr) : layout(shape), raw_ptr(ptr){};
  Tensor(std::vector<size_t> shape) : layout(shape) {
    raw_ptr = new float[layout.size];
  }
  void randn() {
    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_real_distribution<float> uniform_dist(0, 1);
    auto ptr = (float *)raw_ptr;
    for (size_t i = 0; i < layout.size; i++) {
      ptr[i] = uniform_dist(e1);
    }
  }

  void set_zero() {
    auto ptr = (float *)raw_ptr;
    for (size_t i = 0; i < layout.size; i++) {
      ptr[i] = 0.f;
    }
  }
  void set_value(float value) {
    auto ptr = (float *)raw_ptr;
    for (size_t i = 0; i < layout.size; i++) {
      ptr[i] = value;
    }
  }

  void *raw_ptr;
  TensorLayout layout;
};

using TensorLayoutVec = std::vector<TensorLayout>;