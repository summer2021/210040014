cd $(dirname $0)
git submodule sync

git submodule foreach --recursive git reset --hard
git submodule foreach --recursive git clean -fd

git submodule update --init gtest